# Домашнее задание к занятию "4.3. Языки разметки JSON и YAML"





## Обязательная задача 1

Мы выгрузили JSON, который получили через API запрос к нашему сервису:

```
    { "info" : "Sample JSON output from our service\t",
        "elements" :[
            { "name" : "first",
            "type" : "server",
            "ip" : 7175 
            }
            { "name" : "second",
            "type" : "proxy",
            "ip : 71.78.22.43
            }
        ]
    }
```

  Нужно найти и исправить все ошибки, которые допускает наш сервис



- пропущена запятая между элементами массива;
- пропущена закрывающая `"` во втором элементе, ключ `ip`;
- нет кавычек для значения `ip` во втором элементе - это не число, а строка;
- видимо, некорректный ip в первом элементе - вместо 7175 должен быть IP `71.75.X.X` и также в кавычках:


```
{
	"info": "Sample JSON output from our service\t",
	"elements": 
	[
		{
		"name": "first",
		"type": "server",
		"ip": "71.75.x.x"
		},
        	{
		"name": "second",
		"type": "proxy",
		"ip": "71.78 .22 .43"
		}
	]
}

```



## Обязательная задача 2

В прошлый рабочий день мы создавали скрипт, позволяющий опрашивать веб-сервисы и получать их IP. К уже реализованному функционалу нам нужно добавить возможность записи JSON и YAML файлов, описывающих наши сервисы. Формат записи JSON по одному сервису: `{ "имя сервиса" : "его IP"}`. Формат записи YAML по одному сервису: `- имя сервиса: его IP`. Если в момент исполнения скрипта меняется IP у сервиса - он должен так же поменяться в yml и json файле.



### Ваш скрипт:

```python
#!/usr/bin/env python3

import socket
import json
import yaml

results = {}

#get old data from file

with open("services.json", "r") as file:
    services = json.load(file)

for key in services.keys():
    results[key] = socket.gethostbyname(key)
    if services[key] != results[key]:
        print('[ERROR]', key, 'IP mismatch:', services[key], '-', results[key])
    else:
        print(key,'-',results[key])

#write new data to file

with open("services.json", "w") as file:
    json.dump(results, file)

with open("services.yaml", "w") as file:
    yaml.dump(results, file)

exit()
```



### Вывод скрипта при запуске при тестировании:

```
naum@nubu:~$ python3 scr7
drive.google.com - 108.177.14.194
[ERROR] mail.google.com IP mismatch: 108.177.14.18 - 108.177.14.83
[ERROR] google.com IP mismatch: 64.233.164.138 - 64.233.164.113
```



### json-файл(ы), который(е) записал ваш скрипт:

```json
{"drive.google.com": "108.177.14.194", "mail.google.com": "108.177.14.83", "google.com": "64.233.164.113"}
```

### yml-файл(ы), который(е) записал ваш скрипт:

```yaml 
drive.google.com: 108.177.14.194
google.com: 64.233.164.113
mail.google.com: 108.177.14.83
```


