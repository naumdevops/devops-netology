# devops-netology
1. Установлен VirtualBox

2. Установлен  Vagrant

3. Используется стандартный терминал Ubuntu

4. Создана директория, в ней с использованием Vagrant скачан образ и запущена VM Ubuntu 20.04

5. графический интерфейс VirtualBox запущен, под VM выделены ресурсы по умолчанию: 1 cpu (2 proc), 1024 MB RAM, 64 GB HDD, 1 network adapter.

6. Добавление оперативной памяти/ресурсов VM возможно через файл Vagrantfile:
config.vm.provider "virtualbox" do |v|
  v.memory = 2048
  v.cpus = 2
end

7. vagrant ssh успешно отрабатывает, внутри VM выполняются команды

8. ознакомлен с man bash. Длину журнала history можно задать с помощью переменной HISTSIZE (875 строка man bash). Директива ignoreboth использует 2 опции в истории команд bash: 

ignorespace — не сохранять строки начинающиеся с символа <пробел>
ignoredups — не сохранять строки, совпадающие с последней выполненной командой

9. {} используются в механизмах Brace Expansion, Parameter Expansion. также используется для группировки команд.

10. touch file_{1..100000}. создать аналогичным образом 300000 файлов не получается из-за ограничения буфера аргументов.

11. конструкция [[ -d /tmp ]] возвращает 0 или 1 в зависимости от того, существует ли файл tmp и является ли он каталогом

12. commands:

vagrant@vagrant:~$ mkdir /tmp/new_path_directory 

vagrant@vagrant:~$ PATH=/tmp/new_path_directory/:$PATH

vagrant@vagrant:~$ cp /bin/bash /tmp/new_path_directory/

vagrant@vagrant:~$ type -a bash

bash is /tmp/new_path_directory/bash
bash is /usr/bin/bash
bash is /bin/bash

13. at выполняется команды в указанное время, batch - выполняет команды, когда позволяет уровень загрузки системы

14. vagrant halt
