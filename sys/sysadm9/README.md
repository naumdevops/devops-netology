# Домашнее задание к занятию "3.9. Элементы безопасности информационных систем"



1. Установите Bitwarden плагин для браузера. Зарегестрируйтесь и сохраните несколько паролей.

![bitwarden](img/bitwarden2.png)



2. Установите Google authenticator на мобильный телефон. Настройте вход в Bitwarden акаунт через Google authenticator OTP.

![bitwarden](img/bitwarden1.png)



3. Установите apache2, сгенерируйте самоподписанный сертификат, настройте тестовый сайт для работы по HTTPS.

```
vagrant@vagrant:~$ sudo apt-get install apache2
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following additional packages will be installed:
  apache2-bin apache2-data apache2-utils libapr1 libaprutil1 libaprutil1-dbd-sqlite3 libaprutil1-ldap libjansson4 liblua5.2-0 ssl-cert
Suggested packages:
  apache2-doc apache2-suexec-pristine | apache2-suexec-custom www-browser openssl-blacklist
The following NEW packages will be installed:
  apache2 apache2-bin apache2-data apache2-utils libapr1 libaprutil1 libaprutil1-dbd-sqlite3 libaprutil1-ldap libjansson4 liblua5.2-0 ssl-cert
0 upgraded, 11 newly installed, 0 to remove and 55 not upgraded.
Need to get 1518 kB/1865 kB of archives.
After this operation, 8091 kB of additional disk space will be used.
Do you want to continue? [Y/n] y
...

vagrant@vagrant:~$ sudo systemctl status apache2
● apache2.service - The Apache HTTP Server
     Loaded: loaded (/lib/systemd/system/apache2.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2022-03-02 17:09:21 UTC; 4min 35s ago
       Docs: https://httpd.apache.org/docs/2.4/
   Main PID: 2308 (apache2)
      Tasks: 55 (limit: 1071)
     Memory: 5.3M
     CGroup: /system.slice/apache2.service
             ├─2308 /usr/sbin/apache2 -k start
             ├─2310 /usr/sbin/apache2 -k start
             └─2311 /usr/sbin/apache2 -k start
Mar 02 17:09:21 vagrant systemd[1]: Starting The Apache HTTP Server...
Mar 02 17:09:21 vagrant apachectl[2307]: AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 127.0.1.1. Set the 'Ser>
Mar 02 17:09:21 vagrant systemd[1]: Started The Apache HTTP Server.

vagrant@vagrant:~$ sudo a2enmod ssl
vagrant@vagrant:~$ sudo service apache2 restart
vagrant@vagrant:~$ sudo mkdir /var/www/netology
vagrant@vagrant:~$ sudo chown -R $USER:$USER /var/www/netology/
vagrant@vagrant:~$ sudo chmod -R 755 /var/www/netology/
vagrant@vagrant:~$ sudo vi /var/www/netology/index.html
vagrant@vagrant:~$ cat /var/www/netology/index.html 
<html>
    <head>
        <title>Test site</title>
    </head>
    <body>
        <h1>Success! Test site is working!</h1>
    </body>
</html>

vagrant@vagrant:~$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt
Generating a RSA private key
...................................................................................................+++++
....................................+++++
writing new private key to '/etc/ssl/private/apache-selfsigned.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:RU
State or Province Name (full name) [Some-State]:Moscow
Locality Name (eg, city) []:Moscow
Organization Name (eg, company) [Internet Widgits Pty Ltd]:netology
Organizational Unit Name (eg, section) []:edu
Common Name (e.g. server FQDN or YOUR name) []:127.0.0.1

vagrant@vagrant:~$ sudo vi /etc/apache2/sites-available/netology.conf
vagrant@vagrant:~$ sudo cat /etc/apache2/sites-available/netology.conf
<VirtualHost *:443>
    ServerAdmin webmaster@localhost
    ServerName 127.0.0.1
    ServerAlias www.netology
    DocumentRoot /var/www/netology
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    SSLEngine on
    SSLCertificateFile /etc/ssl/certs/apache-selfsigned.crt   
    SSLCertificateKeyFile /etc/ssl/private/apache-selfsigned.key
</VirtualHost>

vagrant@vagrant:~$ sudo a2ensite netology.conf
Enabling site netology.
To activate the new configuration, you need to run:
  systemctl reload apache2
vagrant@vagrant:~$ sudo a2dissite 000-default.conf
Site 000-default disabled.
To activate the new configuration, you need to run:
  systemctl reload apache2
vagrant@vagrant:~$ sudo apache2ctl configtest
Syntax OK
vagrant@vagrant:~$ sudo systemctl restart apache2
```

В vagrant настроен форвардинг порта 443 виртуальной машины на 8443 порт хоста:

![https_apache](img/https_apache.png)



4. Проверьте на TLS уязвимости произвольный сайт в интернете (кроме сайтов МВД, ФСБ, МинОбр, НацБанк, РосКосмос, РосАтом, РосНАНО и любых госкомпаний, объектов КИИ, ВПК ... и тому подобное).



```
vagrant@vagrant:~$ sudo apt install testssl.sh
vagrant@vagrant:~$ testssl -U --sneaky https://netology.ru/
No engine or GOST support via engine with your /usr/bin/openssl
###########################################################
    testssl       3.0 from https://testssl.sh/
      This program is free software. Distribution and
             modification under GPLv2 permitted.
      USAGE w/o ANY WARRANTY. USE IT AT YOUR OWN RISK!
       Please file bugs @ https://testssl.sh/bugs/
###########################################################
 Using "OpenSSL 1.1.1f  31 Mar 2020" [~79 ciphers]
 on vagrant:/usr/bin/openssl
 (built: "Nov 24 13:20:48 2021", platform: "debian-amd64")
Testing all IPv4 addresses (port 443): 172.67.21.207 104.22.41.171 104.22.40.171
----------------------------------------------------------------------------------------------------------
 Start 2022-03-02 17:58:52        -->> 172.67.21.207:443 (netology.ru) <<--
 Further IP addresses:   104.22.40.171 104.22.41.171 2606:4700:10::6816:29ab 2606:4700:10::6816:28ab 2606:4700:10::ac43:15cf 
 rDNS (172.67.21.207):   --
 Service detected:       HTTP
 Testing vulnerabilities 
 Heartbleed (CVE-2014-0160)                not vulnerable (OK), no heartbeat extension
 CCS (CVE-2014-0224)                       not vulnerable (OK)
 Ticketbleed (CVE-2016-9244), experiment.  not vulnerable (OK), no session tickets
 ROBOT                                     not vulnerable (OK)
 Secure Renegotiation (RFC 5746)           OpenSSL handshake didn't succeed
 Secure Client-Initiated Renegotiation     not vulnerable (OK)
 CRIME, TLS (CVE-2012-4929)                not vulnerable (OK)
 BREACH (CVE-2013-3587)                    potentially NOT ok, uses gzip HTTP compression. - only supplied "/" tested
                                           Can be ignored for static pages or if no secrets in the page
 POODLE, SSL (CVE-2014-3566)               not vulnerable (OK)
 TLS_FALLBACK_SCSV (RFC 7507)              No fallback possible (OK), no protocol below TLS 1.2 offered
 SWEET32 (CVE-2016-2183, CVE-2016-6329)    VULNERABLE, uses 64 bit block ciphers
 FREAK (CVE-2015-0204)                     not vulnerable (OK)
 DROWN (CVE-2016-0800, CVE-2016-0703)      not vulnerable on this host and port (OK)
                                           make sure you don't use this certificate elsewhere with SSLv2 enabled services
                                           https://censys.io/ipv4?q=A3C7D9A8D3805171D99EA61F5C80B8ADF49B93BA21EBB492D78512BA254E90A5 could help you to find out
 LOGJAM (CVE-2015-4000), experimental      not vulnerable (OK): no DH EXPORT ciphers, no DH key detected with <= TLS 1.2
 BEAST (CVE-2011-3389)                     not vulnerable (OK), no SSL3 or TLS1 LUCKY13 (CVE-2013-0169), experimental     potentially VULNERABLE, uses cipher block chaining (CBC) ciphers with TLS. Check patches
 RC4 (CVE-2013-2566, CVE-2015-2808)        no RC4 ciphers detected (OK)
 Done 2022-03-02 17:59:26 [  37s] -->> 172.67.21.207:443 (netology.ru) <<--
----------------------------------------------------------------------------------------------------------
 Start 2022-03-02 17:59:26        -->> 104.22.41.171:443 (netology.ru) <<--
 Further IP addresses:   104.22.40.171 172.67.21.207 2606:4700:10::6816:29ab 2606:4700:10::6816:28ab 2606:4700:10::ac43:15cf 
 rDNS (104.22.41.171):   --
 Service detected:       HTTP
 Testing vulnerabilities 
 Heartbleed (CVE-2014-0160)                not vulnerable (OK), no heartbeat extension
 CCS (CVE-2014-0224)                       not vulnerable (OK)
 Ticketbleed (CVE-2016-9244), experiment.  not vulnerable (OK), no session tickets
 ROBOT                                     not vulnerable (OK)
 Secure Renegotiation (RFC 5746)           OpenSSL handshake didn't succeed
 Secure Client-Initiated Renegotiation     not vulnerable (OK)
 CRIME, TLS (CVE-2012-4929)                not vulnerable (OK)
 BREACH (CVE-2013-3587)                    potentially NOT ok, uses gzip HTTP compression. - only supplied "/" tested
                                           Can be ignored for static pages or if no secrets in the page
 POODLE, SSL (CVE-2014-3566)               not vulnerable (OK)
 TLS_FALLBACK_SCSV (RFC 7507)              No fallback possible (OK), no protocol below TLS 1.2 offered
 SWEET32 (CVE-2016-2183, CVE-2016-6329)    VULNERABLE, uses 64 bit block ciphers
 FREAK (CVE-2015-0204)                     not vulnerable (OK)
 DROWN (CVE-2016-0800, CVE-2016-0703)      not vulnerable on this host and port (OK)
                                           make sure you don't use this certificate elsewhere with SSLv2 enabled services
                                           https://censys.io/ipv4?q=A3C7D9A8D3805171D99EA61F5C80B8ADF49B93BA21EBB492D78512BA254E90A5 could help you to find out
 LOGJAM (CVE-2015-4000), experimental      not vulnerable (OK): no DH EXPORT ciphers, no DH key detected with <= TLS 1.2
 BEAST (CVE-2011-3389)                     not vulnerable (OK), no SSL3 or TLS1 LUCKY13 (CVE-2013-0169), experimental     potentially VULNERABLE, uses cipher block chaining (CBC) ciphers with TLS. Check patches
 RC4 (CVE-2013-2566, CVE-2015-2808)        no RC4 ciphers detected (OK)
 Done 2022-03-02 18:00:07 [  78s] -->> 104.22.41.171:443 (netology.ru) <<--
----------------------------------------------------------------------------------------------------------
 Start 2022-03-02 18:00:07        -->> 104.22.40.171:443 (netology.ru) <<--
 Further IP addresses:   104.22.41.171 172.67.21.207 2606:4700:10::6816:29ab 2606:4700:10::6816:28ab 2606:4700:10::ac43:15cf 
 rDNS (104.22.40.171):   --
 Service detected:       HTTP
 Testing vulnerabilities 
 Heartbleed (CVE-2014-0160)                not vulnerable (OK), no heartbeat extension
 CCS (CVE-2014-0224)                       not vulnerable (OK)
 Ticketbleed (CVE-2016-9244), experiment.  not vulnerable (OK), no session tickets
 ROBOT                                     not vulnerable (OK)
 Secure Renegotiation (RFC 5746)           OpenSSL handshake didn't succeed
 Secure Client-Initiated Renegotiation     not vulnerable (OK)
 CRIME, TLS (CVE-2012-4929)                not vulnerable (OK)
 BREACH (CVE-2013-3587)                    potentially NOT ok, uses gzip HTTP compression. - only supplied "/" tested
                                           Can be ignored for static pages or if no secrets in the page
 POODLE, SSL (CVE-2014-3566)               not vulnerable (OK)
 TLS_FALLBACK_SCSV (RFC 7507)              No fallback possible (OK), no protocol below TLS 1.2 offered
 SWEET32 (CVE-2016-2183, CVE-2016-6329)    VULNERABLE, uses 64 bit block ciphers
 FREAK (CVE-2015-0204)                     not vulnerable (OK)
 DROWN (CVE-2016-0800, CVE-2016-0703)      not vulnerable on this host and port (OK)
                                           make sure you don't use this certificate elsewhere with SSLv2 enabled services
                                           https://censys.io/ipv4?q=A3C7D9A8D3805171D99EA61F5C80B8ADF49B93BA21EBB492D78512BA254E90A5 could help you to find out
 LOGJAM (CVE-2015-4000), experimental      not vulnerable (OK): no DH EXPORT ciphers, no DH key detected with <= TLS 1.2
 BEAST (CVE-2011-3389)                     not vulnerable (OK), no SSL3 or TLS1 LUCKY13 (CVE-2013-0169), experimental     potentially VULNERABLE, uses cipher block chaining (CBC) ciphers with TLS. Check patches
 RC4 (CVE-2013-2566, CVE-2015-2808)        no RC4 ciphers detected (OK)
 Done 2022-03-02 18:00:50 [ 121s] -->> 104.22.40.171:443 (netology.ru) <<--
----------------------------------------------------------------------------------------------------------
Done testing now all IP addresses (on port 443): 172.67.21.207 104.22.41.171 104.22.40.171
```

5. Установите на Ubuntu ssh сервер, сгенерируйте новый приватный ключ. Скопируйте свой публичный ключ на другой сервер. Подключитесь к серверу по SSH-ключу.


```
vagrant@vagrant:~$ sudo apt install openssh-server
…
vagrant@vagrant:~$ sudo ufw allow ssh
Rules updated
Rules updated (v6)
vagrant@vagrant:~$ sudo systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2022-03-02 18:41:27 UTC; 1min 29s ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 21363 (sshd)
      Tasks: 1 (limit: 1071)
     Memory: 1.2M
     CGroup: /system.slice/ssh.service
             └─21363 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

Mar 02 18:41:27 vagrant systemd[1]: Starting OpenBSD Secure Shell server...
Mar 02 18:41:27 vagrant sshd[21363]: Server listening on 0.0.0.0 port 22.
Mar 02 18:41:27 vagrant sshd[21363]: Server listening on :: port 22.
Mar 02 18:41:27 vagrant systemd[1]: Started OpenBSD Secure Shell server.

vagrant@vagrant:~$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/vagrant/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/vagrant/.ssh/id_rsa
Your public key has been saved in /home/vagrant/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:+LxvDxs+alFRJLLPG8RcRcHtu6NZbeCvmGSo8l7OQpM vagrant@vagrant
The key's randomart image is:
+---[RSA 3072]----+
|        . .o+++o |
|         =.o  . .|
|        . +.   . |
|       . +.     .|
|      . So+   . .|
|       oE  + . o.|
|       .oo* o ..+|
|      . oBo* oo= |
|       =*=*o+oo.o|
+----[SHA256]-----+

vagrant@vagrant:~$ ssh-copy-id -i /home/vagrant/.ssh/id_rsa.pub naum@10.0.2.2
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/vagrant/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
naum@10.0.2.2's password: 
Number of key(s) added: 1
Now try logging into the machine, with:   "ssh 'naum@10.0.2.2'"
and check to make sure that only the key(s) you wanted were added.

vagrant@vagrant:~$ ssh naum@10.0.2.2
Welcome to Ubuntu 20.04.4 LTS (GNU/Linux 5.13.0-30-generic x86_64)
 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage
3 updates can be applied immediately.
2 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable
2 updates could not be installed automatically. For more details,
see /var/log/unattended-upgrades/unattended-upgrades.log
Your Hardware Enablement Stack (HWE) is supported until April 2025.
*** System restart required ***
Last login: Wed Mar  2 21:50:19 2022 from 127.0.0.1
```

6. Переименуйте файлы ключей из задания 5. Настройте файл конфигурации SSH клиента, так чтобы вход на удаленный сервер осуществлялся по имени сервера.


```
vagrant@vagrant:~$ mv ~/.ssh/id_rsa ~/.ssh/key
vagrant@vagrant:~$ mv ~/.ssh/id_rsa.pub ~/.ssh/pkey
vagrant@vagrant:~$ vi ~/.ssh/config
vagrant@vagrant:~$ cat ~/.ssh/config
Host nubu
User naum
HostName 10.0.2.2
Port 22
IdentityFile ~/.ssh/key

vagrant@vagrant:~$ ssh nubu

Welcome to Ubuntu 20.04.4 LTS (GNU/Linux 5.13.0-30-generic x86_64)
 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

3 updates can be applied immediately.
2 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable
2 updates could not be installed automatically. For more details,
see /var/log/unattended-upgrades/unattended-upgrades.log
Your Hardware Enablement Stack (HWE) is supported until April 2025.
*** System restart required ***
Last login: Wed Mar  2 21:59:25 2022 from 127.0.0.1
```



7. Соберите дамп трафика утилитой tcpdump в формате pcap, 100 пакетов. Откройте файл pcap в Wireshark.


```
vagrant@vagrant:~$ sudo tcpdump -i any -c 100 -w pcap.pcap
tcpdump: listening on any, link-type LINUX_SLL (Linux cooked v1), capture size 262144 bytes
100 packets captured
113 packets received by filter
0 packets dropped by kernel
```

![wireshark](img/wireshark.png)

