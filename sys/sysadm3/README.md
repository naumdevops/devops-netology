# Домашнее задание к занятию "3.3. Операционные системы, лекция 1"



1. Какой системный вызов делает команда `cd`? В прошлом ДЗ мы выяснили, что `cd` не является самостоятельной  программой, это `shell builtin`, поэтому запустить `strace` непосредственно на `cd` не получится. Тем не менее, вы можете запустить `strace` на `/bin/bash -c 'cd /tmp'`. В этом случае вы увидите полный список системных вызовов, которые делает сам `bash` при старте. Вам нужно найти тот единственный, который относится именно к `cd`. Обратите внимание, что `strace` выдаёт результат своей работы в поток stderr, а не в stdout.

`chdir("/tmp")                           = 0`


2. Попробуйте использовать команду `file` на объекты разных типов на файловой системе. Например:

    ```bash
    vagrant@netology1:~$ file /dev/tty
    /dev/tty: character special (5/0)
    vagrant@netology1:~$ file /dev/sda
    /dev/sda: block special (8/0)
    vagrant@netology1:~$ file /bin/bash
    /bin/bash: ELF 64-bit LSB shared object, x86-64
    ```

    Используя `strace` выясните, где находится база данных `file` на основании которой она делает свои догадки.

`/usr/share/misc/magic.mgc`

3. Предположим, приложение пишет лог в текстовый файл. Этот файл оказался удален (deleted в lsof), однако возможности сигналом сказать приложению переоткрыть файлы или просто перезапустить приложение – нет. Так как приложение продолжает писать в удаленный файл, место на диске постепенно заканчивается. Основываясь на знаниях о перенаправлении потоков предложите способ обнуления открытого удаленного файла (чтобы освободить место на файловой системе).

```
vagrant@vagrant:~$ ping -i 5 localhost >> log &
[1] 1231
vagrant@vagrant:~$ cat log
PING localhost (127.0.0.1) 56(84) bytes of data.
64 bytes from localhost (127.0.0.1): icmp_seq=1 ttl=64 time=0.028 ms
vagrant@vagrant:~$ rm log 
vagrant@vagrant:~$ sudo lsof -p 1231 | grep log
ping    1231 vagrant    1w   REG  253,0      325 1054692 /home/vagrant/log (deleted)
vagrant@vagrant:~$ echo "" | sudo tee /proc/1231/fd/1
vagrant@vagrant:~$ sudo lsof -p 1231 | grep log
ping    1231 vagrant    1w   REG  253,0        1 1054692 /home/vagrant/log (deleted)
vagrant@vagrant:~$ sudo lsof -p 1231 | grep log
ping    1231 vagrant    1w   REG  253,0       71 1054692 /home/vagrant/log (deleted)
vagrant@vagrant:~$ kill 1231
```


4. Занимают ли зомби-процессы какие-то ресурсы в ОС (CPU, RAM, IO)?

Зомби-процессы освобождают ресурсы, но занимают запись в таблице процессов.

5. В iovisor BCC есть утилита `opensnoop`:

    ```bash
    root@vagrant:~# dpkg -L bpfcc-tools | grep sbin/opensnoop
    /usr/sbin/opensnoop-bpfcc
    ```

    На какие файлы вы увидели вызовы группы `open` за первую секунду работы утилиты? Воспользуйтесь пакетом `bpfcc-tools` для Ubuntu 20.04. Дополнительные [сведения по установке](https://github.com/iovisor/bcc/blob/master/INSTALL.md).

```
vagrant@vagrant:~$ sudo /usr/sbin/opensnoop-bpfcc 
PID    COMM               FD ERR PATH
942    vminfo              4   0 /var/run/utmp
635    dbus-daemon        -1   2 /usr/local/share/dbus-1/system-services
635    dbus-daemon        19   0 /usr/share/dbus-1/system-services
635    dbus-daemon        -1   2 /lib/dbus-1/system-services
635    dbus-daemon        19   0 /var/lib/snapd/dbus-1/system-services/
645    irqbalance          6   0 /proc/interrupts
645    irqbalance          6   0 /proc/stat
645    irqbalance          6   0 /proc/irq/20/smp_affinity
645    irqbalance          6   0 /proc/irq/0/smp_affinity
645    irqbalance          6   0 /proc/irq/1/smp_affinity
```

6. Какой системный вызов использует `uname -a`? Приведите цитату из man по этому системному вызову, где описывается альтернативное местоположение в `/proc`, где можно узнать версию ядра и релиз ОС.

`uname({sysname="Linux", nodename="vagrant", ...}) = 0`

`Part of the utsname information is also accessible via /proc/sys/kernel/{ostype,  hostname,  osrelease, version, domainname}`.

7. Чем отличается последовательность команд через `;` и через `&&` в bash? Например:

    ```bash
    root@netology1:~# test -d /tmp/some_dir; echo Hi
    Hi
    root@netology1:~# test -d /tmp/some_dir && echo Hi
    root@netology1:~#
    ```

    Есть ли смысл использовать в bash `&&`, если применить `set -e`?

В случае `;` вторая команда будет выполнена вне зависимости от того, как выполнится первая.
В случае `&&` вторая команда будет выполнена только в случае успешного выполнения первой.

`set -e  Exit immediately if a command exits with a non-zero status.` - смысла использовать && нет.

8. Из каких опций состоит режим bash `set -euxo pipefail` и почему его хорошо было бы использовать в сценариях?

`-e` - прерывает выполнение, если команда вернула ненулевой статус 
`-u` - считает неустановленные переменные как ошибки
`-x` - выводит команды с параметрами после подстановок и перед их выполнением
`-o` pipefail возвращает статус последней команды, которая вернула ненулевой статус или 0, если таких команд нет

Полезно для отладки.

9. Используя `-o stat` для `ps`, определите, какой наиболее часто встречающийся статус у процессов в системе. В `man ps` ознакомьтесь (`/PROCESS STATE CODES`) что значат дополнительные к основной заглавной буквы статуса процессов. Его можно не учитывать при расчете (считать S, Ss или Ssl равнозначными).

```
vagrant@vagrant:~$ ps -o stat
STAT
Ss
R+
```

```
PROCESS STATE CODES

       Here are the different values that the s, stat and state output specifiers (header "STAT" or "S") will display to describe the state of a process:

               D    uninterruptible sleep (usually IO)
               I    Idle kernel thread
               R    running or runnable (on run queue)
               S    interruptible sleep (waiting for an event to complete)
               T    stopped by job control signal
               t    stopped by debugger during the tracing
               W    paging (not valid since the 2.6.xx kernel)
               X    dead (should never be seen)
               Z    defunct ("zombie") process, terminated but not reaped by its parent

       For BSD formats and when the stat keyword is used, additional characters may be displayed:

               <    high-priority (not nice to other users)
               N    low-priority (nice to other users)
               L    has pages locked into memory (for real-time and custom IO)
               s    is a session leader
               l    is multi-threaded (using CLONE_THREAD, like NPTL pthreads do)
               +    is in the foreground process group
```

