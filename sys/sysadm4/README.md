# Домашнее задание к занятию "3.4. Операционные системы, лекция 2"



1. На лекции мы познакомились с [node_exporter](https://github.com/prometheus/node_exporter/releases). В демонстрации его исполняемый файл запускался в background. Этого достаточно для демо, но не для настоящей production-системы, где процессы должны находиться под внешним управлением. Используя знания из лекции по systemd, создайте самостоятельно простой [unit-файл](https://www.freedesktop.org/software/systemd/man/systemd.service.html) для node_exporter:

    * поместите его в автозагрузку,
    * предусмотрите возможность добавления опций к запускаемому процессу через внешний файл (посмотрите, например, на `systemctl cat cron`),
    * удостоверьтесь, что с помощью systemctl процесс корректно стартует, завершается, а после перезагрузки автоматически поднимается.

```
vagrant@vagrant:~$ wget https://github.com/prometheus/node_exporter/releases/download/v1.3.1/node_exporter-1.3.1.linux-amd64.tar.gz
vagrant@vagrant:~$ tar xzf node_exporter-1.3.1.linux-amd64.tar.gz 
vagrant@vagrant:~$ sudo mv -v node_exporter-1.3.1.linux-amd64/node_exporter /usr/local/bin/
renamed 'node_exporter-1.3.1.linux-amd64/node_exporter' -> '/usr/local/bin/node_exporter'
vagrant@vagrant:~$ sudo touch /etc/default/node_exporter
vagrant@vagrant:~$ sudo cat /etc/systemd/system/node_exporter.service
[Unit]
Description=Node Exporter 

[Service]
EnvironmentFile=/etc/default/node_exporter
ExecStart=/usr/local/bin/node_exporter $EXTRA_OPTS 
Restart=on-failure

[Install]
WantedBy=multi-user.target

vagrant@vagrant:~$ sudo systemctl daemon-reload
vagrant@vagrant:~$ sudo systemctl enable node_exporter
Created symlink /etc/systemd/system/multi-user.target.wants/node_exporter.service → /etc/systemd/system/node_exporter.service.
vagrant@vagrant:~$ sudo systemctl start node_exporter
vagrant@vagrant:~$ sudo systemctl status node_exporter
● node_exporter.service - Node Exporter
     Loaded: loaded (/etc/systemd/system/node_exporter.service; disabled; vendor preset: enabled)
     Active: active (running) since Mon 2022-02-07 16:38:41 UTC; 58s ago
   Main PID: 2098 (node_exporter)
      Tasks: 4 (limit: 1071)
     Memory: 2.4M
     CGroup: /system.slice/node_exporter.service
             └─2098 /usr/local/bin/node_exporter

Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:115 level=info collector=thermal_zone
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:115 level=info collector=time
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:115 level=info collector=timex
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:115 level=info collector=udp_queues
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:115 level=info collector=uname
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:115 level=info collector=vmstat
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:115 level=info collector=xfs
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:115 level=info collector=zfs
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:199 level=info msg="Listening on" address=:9100
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=tls_config.go:195 level=info msg="TLS is disabled." http2=false

vagrant@vagrant:~$ sudo systemctl stop node_exporter
vagrant@vagrant:~$ sudo systemctl status node_exporter
● node_exporter.service - Node Exporter
     Loaded: loaded (/etc/systemd/system/node_exporter.service; disabled; vendor preset: enabled)
     Active: inactive (dead)
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:115 level=info collector=udp_queues
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:115 level=info collector=uname
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:115 level=info collector=vmstat
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:115 level=info collector=xfs
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:115 level=info collector=zfs
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=node_exporter.go:199 level=info msg="Listening on" address=:9100
Feb 07 16:38:41 vagrant node_exporter[2098]: ts=2022-02-07T16:38:41.817Z caller=tls_config.go:195 level=info msg="TLS is disabled." http2=false
Feb 07 16:40:46 vagrant systemd[1]: Stopping Node Exporter...
Feb 07 16:40:46 vagrant systemd[1]: node_exporter.service: Succeeded.
Feb 07 16:40:46 vagrant systemd[1]: Stopped Node Exporter.
```
После перезагрузки виртуальной машины сервис поднимается автоматически.



2. Ознакомьтесь с опциями node_exporter и выводом `/metrics` по-умолчанию. Приведите несколько опций, которые вы бы выбрали для базового мониторинга хоста по CPU, памяти, диску и сети.

Вывод по умолчанию:
```
vagrant@vagrant:~$ curl http://localhost:9100/metrics
# HELP go_gc_duration_seconds A summary of the pause duration of garbage collection cycles.
# TYPE go_gc_duration_seconds summary
go_gc_duration_seconds{quantile="0"} 7.4485e-05
go_gc_duration_seconds{quantile="0.25"} 0.000184973
go_gc_duration_seconds{quantile="0.5"} 0.000259069
...
```

CPU:
```
node_cpu_seconds_total{cpu="0",mode="idle"} 5369.54
node_cpu_seconds_total{cpu="0",mode="iowait"} 1.38
node_cpu_seconds_total{cpu="0",mode="system"} 12.08
process_cpu_seconds_total 0.66
```

RAM:
```
node_memory_MemAvailable_bytes 7.20982016e+08
node_memory_MemFree_bytes 2.5419776e+08
node_memory_SwapFree_bytes 2.057302016e+09
node_memory_SwapTotal_bytes 2.057302016e+09
```

диски:
```
node_filesystem_avail_bytes{device="/dev/mapper/ubuntu--vg-ubuntu--lv",fstype="ext4",mountpoint="/"} 2.710904832e+10
node_filesystem_size_bytes{device="/dev/mapper/ubuntu--vg-ubuntu--lv",fstype="ext4",mountpoint="/"} 3.315785728e+10
node_filesystem_files_free{device="/dev/mapper/ubuntu--vg-ubuntu--lv",fstype="ext4",mountpoint="/"} 1.990795e+06
node_filesystem_files{device="/dev/mapper/ubuntu--vg-ubuntu--lv",fstype="ext4",mountpoint="/"} 2.064384e+06
node_disk_io_now{device="sda"} 0
```

сеть:
```
node_network_receive_errs_total{device="eth0"} 0
node_network_transmit_errs_total{device="eth0"} 0
node_network_receive_bytes_total{device="eth0"} 140085
node_network_transmit_bytes_total{device="eth0"} 174608
```


3. Установите в свою виртуальную машину [Netdata](https://github.com/netdata/netdata). Воспользуйтесь [готовыми пакетами](https://packagecloud.io/netdata/netdata/install) для установки (`sudo apt install -y netdata`). После успешной установки:

    * в конфигурационном файле `/etc/netdata/netdata.conf` в секции [web] замените значение с localhost на `bind to = 0.0.0.0`,
    * добавьте в Vagrantfile проброс порта Netdata на свой локальный компьютер и сделайте `vagrant reload`:
    ```bash
    config.vm.network "forwarded_port", guest: 19999, host: 19999
    ```

    После успешной перезагрузки в браузере *на своем ПК* (не в виртуальной машине) вы должны суметь зайти на `localhost:19999`. Ознакомьтесь с метриками, которые по умолчанию собираются Netdata и с комментариями, которые даны к этим метрикам.

netdata установлен, конфигурационный файл изменен, проброс порта в конфигурационном файле Vagrantfile прописан. Ниже - скрин с локальной машины (не виртуальной).

![screen](img/screen.png)

4. Можно ли по выводу `dmesg` понять, осознает ли ОС, что загружена не на настоящем оборудовании, а на системе виртуализации?

```
vagrant@vagrant:~$ dmesg | grep "DMI"
[    0.000000] DMI: innotek GmbH VirtualBox/VirtualBox, BIOS VirtualBox 12/01/2006

```

5. Как настроен sysctl `fs.nr_open` на системе по-умолчанию? Узнайте, что означает этот параметр. Какой другой существующий лимит не позволит достичь такого числа (`ulimit --help`)?

```
vagrant@vagrant:~$ sysctl fs.nr_open
fs.nr_open = 1048576
```

Этот параметр обозначает максимальное количество открытых дескрипторов файлов в системе
Не позволит достичь такого числа мягкий лимит пользователя на открытые дескрипторы файлов (может быть увеличен) и жесткий лимит (не может превышать системный):

```
vagrant@vagrant:~$ ulimit -Sn
1024
vagrant@vagrant:~$ ulimit -Hn
1048576
```


6. Запустите любой долгоживущий процесс (не `ls`, который отработает мгновенно, а, например, `sleep 1h`) в отдельном неймспейсе процессов; покажите, что ваш процесс работает под PID 1 через `nsenter`. Для простоты работайте в данном задании под root (`sudo -i`). Под обычным пользователем требуются дополнительные опции (`--map-root-user`) и т.д.

Первый терминал:

```
vagrant@vagrant:~$ sudo unshare -fp --mount-proc ping localhost -i 5
PING localhost (127.0.0.1) 56(84) bytes of data.
64 bytes from localhost (127.0.0.1): icmp_seq=1 ttl=64 time=0.015 ms
64 bytes from localhost (127.0.0.1): icmp_seq=2 ttl=64 time=0.043 ms
64 bytes from localhost (127.0.0.1): icmp_seq=3 ttl=64 time=0.362 ms
64 bytes from localhost (127.0.0.1): icmp_seq=4 ttl=64 time=0.030 ms
64 bytes from localhost (127.0.0.1): icmp_seq=5 ttl=64 time=0.040 ms
```

Второй терминал:

```
vagrant@vagrant:~$ sudo -i
root@vagrant:~# ps -e | grep ping
   6967 pts/0    00:00:00 ping
root@vagrant:~# nsenter -t 6967 -p -m
root@vagrant:/# ps -e
    PID TTY          TIME CMD
      1 pts/0    00:00:00 ping
      2 pts/1    00:00:00 bash
     13 pts/1    00:00:00 ps
```


7. Найдите информацию о том, что такое `:(){ :|:& };:`. Запустите эту команду в своей виртуальной машине Vagrant с Ubuntu 20.04 (**это важно, поведение в других ОС не проверялось**). Некоторое время все будет "плохо", после чего (минуты) – ОС должна стабилизироваться. Вызов `dmesg` расскажет, какой механизм помог автоматической стабилизации. Как настроен этот механизм по-умолчанию, и как изменить число процессов, которое можно создать в сессии?

`:()` означает функцию, называемую `:`

`{:|: &}` означает запустить функцию `:` и снова отправить ее вывод в функцию `:` и запустить ее в фоновом режиме.

Символ `;` является разделителем команд.

`:` запускает функцию в первый раз.

Создается функция, которая вызывает себя дважды при каждом вызове и не имеет возможности завершить себя. Он будет продолжать удваиваться до тех пор, пока у вас не кончатся системные ресурсы.

```
vagrant@vagrant:~$ :(){ :|:& };:
-bash: fork: retry: Resource temporarily unavailable
-bash: fork: retry: Resource temporarily unavailable
-bash: fork: retry: Resource temporarily unavailable
-bash: fork: retry: Resource temporarily unavailable
…
-bash: fork: retry: Resource temporarily unavailable
-bash: fork: Resource temporarily unavailable
-bash: fork: Resource temporarily unavailable
-bash: fork: Resource temporarily unavailable
-bash: fork: Resource temporarily unavailable
[1]+  Done                    : | :
```

dmesg:
`[ 2029.768281] cgroup: fork rejected by pids controller in /user.slice/user-1000.slice/session-3.scope`

Systemd создает группу для каждого пользователя, и все процессы пользователя принадлежат одной и той же группе.
Cgroups - это механизм Linux, устанавливающий ограничения на системные ресурсы, такие как максимальное количество процессов, циклы ЦП, использование ОЗУ и т. д.
По умолчанию максимальное количество задач, которое systemd разрешает для каждого пользователя, составляет 33% от «общесистемного максимума». 
пользователь по умолчанию устанавливается через TasksMax в файле:
`/usr/lib/systemd/system/user-.slice.d/10-defaults.conf`


Чтобы настроить ограничение для конкретного пользователя (которое будет применено сразу же, а также сохранено в /etc/systemd/system.control), нужно выполнить:

`systemctl [--runtime] set-property user-<uid>.slice TasksMax=<value>`

 

 ---


