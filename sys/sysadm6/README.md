# Домашнее задание к занятию "3.6. Компьютерные сети, лекция 1"



1. Работа c HTTP через телнет.

- Подключитесь утилитой телнет к сайту stackoverflow.com

`telnet stackoverflow.com 80`

- отправьте HTTP запрос

```bash

GET /questions HTTP/1.0

HOST: stackoverflow.com

[press enter]

[press enter]

```

- В ответе укажите полученный HTTP код, что он означает?


```
naum@nubu:~$ telnet stackoverflow.com 80
Trying 151.101.193.69...
Connected to stackoverflow.com.
Escape character is '^]'.
GET /questions HTTP/1.0
HOST: stackoverflow.com

HTTP/1.1 301 Moved Permanently
cache-control: no-cache, no-store, must-revalidate
location: https://stackoverflow.com/questions
x-request-guid: 9188e25a-0318-4452-8b07-be09c605aa37
feature-policy: microphone 'none'; speaker 'none'
content-security-policy: upgrade-insecure-requests; frame-ancestors 'self' https://stackexchange.com
Accept-Ranges: bytes
Date: Mon, 14 Feb 2022 12:28:00 GMT
Via: 1.1 varnish
Connection: close
X-Served-By: cache-fra19140-FRA
X-Cache: MISS
X-Cache-Hits: 0
X-Timer: S1644841680.015276,VS0,VE85
Vary: Fastly-SSL
X-DNS-Prefetch-Control: off
Set-Cookie: prov=0197db12-99a3-e9d2-e1d9-7c6f5ece7a3e; domain=.stackoverflow.com; expires=Fri, 01-Jan-2055 00:00:00 GMT; path=/; HttpOnly
Connection closed by foreign host.
```

Код перенаправления "301 Moved Permanently" протокола передачи гипертекста (HTTP) показывает, что запрошенный ресурс был окончательно перемещён в URL, указанный в заголовке Location (en-US). Браузер в случае такого ответа перенаправляется на эту страницу, а поисковые системы обновляют свои ссылки на ресурс.


2. Повторите задание 1 в браузере, используя консоль разработчика F12.

- откройте вкладку `Network`

- отправьте запрос http://stackoverflow.com

- найдите первый ответ HTTP сервера, откройте вкладку `Headers`

- укажите в ответе полученный HTTP код.

- проверьте время загрузки страницы, какой запрос обрабатывался дольше всего?

- приложите скриншот консоли браузера в ответ.



200 ОК. 

Дольше всего происходила загрузка страницы.

![tty](img/webconsole1.png)

![tty](img/webconsole2.png)


3. Какой IP адрес у вас в интернете?

62.76.84.193

4. Какому провайдеру принадлежит ваш IP адрес? Какой автономной системе AS? Воспользуйтесь утилитой `whois`

LLC CTI

```
naum@nubu:~$ whois  62.76.84.193 | grep descr
descr:          LLC CTI
descr:          LLC CTI AS route
naum@nubu:~$ whois  62.76.84.193 | grep origin
origin:         AS60473
```


5. Через какие сети проходит пакет, отправленный с вашего компьютера на адрес 8.8.8.8? Через какие AS? Воспользуйтесь утилитой `traceroute`

```
naum@nubu:~$ traceroute 8.8.8.8 -An
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  172.16.34.254 [*]  2.910 ms  2.984 ms  3.096 ms
 2  172.16.6.254 [*]  1.951 ms  1.988 ms  1.987 ms
 3  62.76.84.246 [AS60473]  2.368 ms  2.520 ms  2.663 ms
 4  172.18.113.2 [*]  2.828 ms  2.819 ms  2.815 ms
 5  82.204.243.197 [AS8359]  4.801 ms  4.805 ms  4.783 ms
 6  195.34.50.182 [AS8359]  4.784 ms  3.003 ms  2.986 ms
 7  212.188.56.13 [AS8359]  6.930 ms  8.459 ms  6.454 ms
 8  195.34.50.74 [AS8359]  3.842 ms  3.710 ms  3.634 ms
 9  212.188.29.82 [AS8359]  3.677 ms  3.622 ms  3.641 ms
10  108.170.250.51 [AS15169]  3.873 ms 108.170.250.34 [AS15169]  4.963 ms *
11  209.85.249.158 [AS15169]  18.543 ms 142.251.49.24 [AS15169]  17.231 ms  16.425 ms
12  108.170.235.64 [AS15169]  19.490 ms 216.239.43.20 [AS15169]  18.682 ms 72.14.235.69 [AS15169]  17.805 ms
13  142.250.56.127 [AS15169]  19.840 ms 216.239.62.9 [AS15169]  22.094 ms 209.85.251.63 [AS15169]  19.468 ms
14  * * *
15  * * *
16  * * *
17  * * *
18  * * *
19  * * *
20  * * *
21  * * *
22  8.8.8.8 [AS15169]  16.320 ms *  19.233 ms
```

6. Повторите задание 5 в утилите `mtr`. На каком участке наибольшая задержка - delay?

```
naum@nubu:~$ mtr 8.8.8.8 -nzc 1 -r
Start: 2022-02-14T15:34:09+0300
HOST: nubu                        Loss%   Snt   Last   Avg  Best  Wrst StDev
  1. AS???    172.16.34.254        0.0%     1    3.8   3.8   3.8   3.8   0.0
  2. AS???    172.16.6.254         0.0%     1    0.8   0.8   0.8   0.8   0.0
  3. AS60473  62.76.84.246         0.0%     1    1.4   1.4   1.4   1.4   0.0
  4. AS???    172.18.113.2         0.0%     1    1.7   1.7   1.7   1.7   0.0
  5. AS8359   82.204.243.197       0.0%     1    2.4   2.4   2.4   2.4   0.0
  6. AS8359   195.34.50.182        0.0%     1    2.4   2.4   2.4   2.4   0.0
  7. AS???    ???                 100.0     1    0.0   0.0   0.0   0.0   0.0
  8. AS8359   195.34.50.74         0.0%     1    3.4   3.4   3.4   3.4   0.0
  9. AS8359   212.188.29.82        0.0%     1    2.9   2.9   2.9   2.9   0.0
 10. AS15169  108.170.250.99       0.0%     1    4.3   4.3   4.3   4.3   0.0
 11. AS15169  142.251.49.24        0.0%     1   15.8  15.8  15.8  15.8   0.0
 12. AS15169  209.85.254.20        0.0%     1   18.9  18.9  18.9  18.9   0.0
 13. AS15169  172.253.64.53        0.0%     1   17.0  17.0  17.0  17.0   0.0
 14. AS???    ???                 100.0     1    0.0   0.0   0.0   0.0   0.0
 15. AS???    ???                 100.0     1    0.0   0.0   0.0   0.0   0.0
 16. AS???    ???                 100.0     1    0.0   0.0   0.0   0.0   0.0
 17. AS???    ???                 100.0     1    0.0   0.0   0.0   0.0   0.0
 18. AS???    ???                 100.0     1    0.0   0.0   0.0   0.0   0.0
 19. AS???    ???                 100.0     1    0.0   0.0   0.0   0.0   0.0
 20. AS???    ???                 100.0     1    0.0   0.0   0.0   0.0   0.0
 21. AS???    ???                 100.0     1    0.0   0.0   0.0   0.0   0.0
 22. AS???    ???                 100.0     1    0.0   0.0   0.0   0.0   0.0
 23. AS???    ???                 100.0     1    0.0   0.0   0.0   0.0   0.0
 24. AS15169  8.8.8.8              0.0%     1   15.8  15.8  15.8  15.8   0.0
```

Наибольшая задержка на узле 12.

7. Какие DNS сервера отвечают за доменное имя dns.google? Какие A записи? воспользуйтесь утилитой `dig`

```
naum@nubu:~$ dig  +short NS dns.google
ns2.zdns.google.
ns1.zdns.google.
ns3.zdns.google.
ns4.zdns.google.
naum@nubu:~$ dig A dns.google +noall +answer
dns.google.		586	IN	A	8.8.4.4
dns.google.		586	IN	A	8.8.8.8
```

8. Проверьте PTR записи для IP адресов из задания 7. Какое доменное имя привязано к IP? воспользуйтесь утилитой `dig`

```
naum@nubu:~$ dig +noall +answer -x 8.8.4.4
4.4.8.8.in-addr.arpa.	7196	IN	PTR	dns.google.
naum@nubu:~$ dig +noall +answer -x 8.8.8.8
8.8.8.8.in-addr.arpa.	15392	IN	PTR	dns.google.
```
