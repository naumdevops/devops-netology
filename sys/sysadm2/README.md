# Домашнее задание к занятию "3.2. Работа в терминале, лекция 2"

1. Какого типа команда cd? Попробуйте объяснить, почему она именно такого типа; опишите ход своих мыслей, если считаете что она могла бы быть другого типа.

cd - встроенная команда оболочки, так как она меняет текущую рабочую директорию. она не может быть внешней. так как нет механизма, позволяющего изменить рабочую директорию другого процесса.

2. Какая альтернатива без pipe команде `grep <some_string> <some_file> | wc -l`? `man grep` поможет в ответе на этот вопрос. Ознакомьтесь с документом о других подобных некорректных вариантах использования pipe. 

`grep` с использованием -с:

```
vagrant@vagrant:~$ echo "newline" >> some_file
vagrant@vagrant:~$ echo "newline" >> some_file
vagrant@vagrant:~$ cat some_file
newline
newline
vagrant@vagrant:~$ grep newline some_file | wc -l
2
vagrant@vagrant:~$ grep newline some_file -c
2
```
3. Какой процесс с PID `1` является родителем для всех процессов в вашей виртуальной машине Ubuntu 20.04?

процесс systemd:

```
vagrant@vagrant:~$ pstree -p
systemd(1)─┬─VBoxService(905)─┬─{VBoxService}(907)
           │                  ├─{VBoxService}(908)
           │                  ├─{VBoxService}(912)
           │                  ├─{VBoxService}(913)
           │                  ├─{VBoxService}(914)
           │                  ├─{VBoxService}(915)
           │                  ├─{VBoxService}(916)
```

4. Как будет выглядеть команда, которая перенаправит вывод stderr `ls` на другую сессию терминала?

Первый открытый терминал:
```
vagrant@vagrant:~$ who
vagrant  pts/0        2022-01-29 14:11 (10.0.2.2)
vagrant  pts/1        2022-01-29 14:15 (10.0.2.2)
vagrant@vagrant:~$ ls no_existing 2>/dev/pts/1
```

Второй открытый терминал:
```
vagrant@vagrant:~$ who
vagrant  pts/0        2022-01-29 14:11 (10.0.2.2)
vagrant  pts/1        2022-01-29 14:15 (10.0.2.2)
vagrant@vagrant:~$ tty 
/dev/pts/1
vagrant@vagrant:~$ ls: cannot access 'no_existing': No such file or directory
```
5. Получится ли одновременно передать команде файл на stdin и вывести ее stdout в другой файл? Приведите работающий пример.

```
vagrant@vagrant:~$ echo "newline" >> file_in
vagrant@vagrant:~$ cat < file_in > file_out
vagrant@vagrant:~$ cat file_out 
newline
```

6. Получится ли находясь в графическом режиме, вывести данные из PTY в какой-либо из эмуляторов TTY? Сможете ли вы наблюдать выводимые данные?

```
vagrant@vagrant:~$ who
vagrant  tty1         2022-01-29 14:23
vagrant  pts/0        2022-01-29 14:11 (10.0.2.2)
vagrant  pts/1        2022-01-29 14:15 (10.0.2.2)
vagrant@vagrant:~$ tty 
/dev/pts/0
vagrant@vagrant:~$ echo "newline" > /dev/tty1
```

![tty](img/tty.png)

7. Выполните команду `bash 5>&1`. К чему она приведет? Что будет, если вы выполните `echo netology > /proc/$$/fd/5`? Почему так происходит?

```
vagrant@vagrant:~$ bash 5>&1
vagrant@vagrant:~$ echo netology > /proc/$$/fd/5
netology
```
`bash 5>&1` создаст новый процесс с дескриптором 5 и перенаправит его в stdout 

`echo netology > /proc/$$/fd/5`  - вызовет echo и перенаправит вывода в процесс с дескриптором 5.

8. Получится ли в качестве входного потока для pipe использовать только stderr команды, не потеряв при этом отображение stdout на pty? Напоминаем: по умолчанию через pipe передается только stdout команды слева от `|` на stdin команды справа.

Это можно сделать, поменяв стандартные потоки местами через промежуточный новый дескриптор, который вы научились создавать в предыдущем вопросе.

```
vagrant@vagrant:~$ cat newline 5>&2 2>&1 1>&5 | grep newline -c
1
```

9. Что выведет команда `cat /proc/$$/environ`? Как еще можно получить аналогичный по содержанию вывод?

`cat /proc/$$/environ` выведет переменные окружения текущего процесса. похожий вывод можно получить с использованием `env`.

10. Используя `man`, опишите что доступно по адресам `/proc/<PID>/cmdline`, `/proc/<PID>/exe`.

`/proc/[pid]/cmdline` содержит полный список команд для процесса, если процесс не зомби

`/proc/[pid]/exe` – содержит symbolic link до исполняемого файла процесса. 

11. Узнайте, какую наиболее старшую версию набора инструкций SSE поддерживает ваш процессор с помощью `/proc/cpuinfo`.

`vagrant@vagrant:~$ cat /proc/cpuinfo | grep sse` sse4_2

12. При открытии нового окна терминала и `vagrant ssh` создается новая сессия и выделяется pty. Это можно подтвердить командой `tty`, которая упоминалась в лекции 3.2. Однако:
    ```bash
	vagrant@netology1:~$ ssh localhost 'tty'
	not a tty
    ```
	Почитайте, почему так происходит, и как изменить поведение.

Если при использовании `ssh` указана команда, которую необходимо выполнить, команда выполняется в неинтерактивной сессии. Интерактивная сессия требует псевдотерминала. Для изменения поведения по умолчанию, необходимо указать флаг `-t`: 

```
vagrant@vagrant:~$ ssh -t localhost 'tty'
vagrant@localhost's password: 
/dev/pts/2
Connection to localhost closed.
```
13. Бывает, что есть необходимость переместить запущенный процесс из одной сессии в другую. Попробуйте сделать это, воспользовавшись `reptyr`. Например, так можно перенести в `screen` процесс, который вы запустили по ошибке в обычной SSH-сессии.

```
vagrant@vagrant:~$ who
vagrant  tty1         2022-01-29 14:23
vagrant  pts/0        2022-01-29 15:58 (10.0.2.2)
vagrant  pts/2        2022-01-29 16:07 (10.0.2.2)
vagrant@vagrant:~$ ps -a
    PID TTY          TIME CMD
   1901 tty1     00:00:00 bash
   2878 pts/2    00:00:00 top
   2897 pts/0    00:00:00 ps
vagrant@vagrant:~$ tty
/dev/pts/0
vagrant@vagrant:~$ sudo reptyr -T 2878
```

14. `sudo echo string > /root/new_file` не даст выполнить перенаправление под обычным пользователем, так как перенаправлением занимается процесс shell'а, который запущен без `sudo` под вашим пользователем. Для решения данной проблемы можно использовать конструкцию `echo string | sudo tee /root/new_file`. Узнайте что делает команда `tee` и почему в отличие от `sudo echo` команда с `sudo tee` будет работать.

tee - read from standard input and write to standard output and files

`tee` получает вывод `echo` перенаправленный через pipe, создает файл в директории root (так как запущена под sudo) и выводит результат в stdout


