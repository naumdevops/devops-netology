# Домашнее задание к занятию "3.7. Компьютерные сети, лекция 2"



1. Проверьте список доступных сетевых интерфейсов на вашем компьютере. Какие команды есть для этого в Linux и в Windows?


```
vagrant@vagrant:~$ ip link show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 08:00:27:b1:28:5d brd ff:ff:ff:ff:ff:ff
```


В Linux есть команды - `ip link show`, `ip a`, `ifconfig -a`, `netstat -i`. 
В Windows - `ipconfig /all`, `netsh interface ipv4 show interfaces`,


2. Какой протокол используется для распознавания соседа по сетевому интерфейсу? Какой пакет и команды есть в Linux для этого?


- Cisco Discovery Protocol (CDP) в сетях Cisco - проприетарный протокол

- Link Layer Discovery Protocol (LLDP) - формализованный в стандарте IEEE 802.1AB протокол.

В Linux есть пакеты lldpd, lldpad, могут быть использованы команды lldpctl, lldptool. 


3. Какая технология используется для разделения L2 коммутатора на несколько виртуальных сетей? Какой пакет и команды есть в Linux для этого? Приведите пример конфига.


Для разделения на несколько виртуальных сетей используются VLAN.
В Linux устанавливается пакет vlan. Могут быть использованы команды vconfig, ip.


```
vagrant@vagrant:~$ sudo vconfig add eth0 15
vagrant@vagrant:~$ sudo ifconfig eth0.15 192.168.0.15 netmask 255.255.255.0 broadcast 192.168.0.255 up
```

Также может быть использована команда ip:

```
vagrant@vagrant:~$ sudo ip link add link eth0 name eth0.5 type vlan id 5
vagrant@vagrant:~$ sudo ip addr add 192.168.1.15/24 brd 192.168.1.255 dev eth0.5
vagrant@vagrant:~$ sudo ip link set dev eth0.5 up
```

```
vagrant@vagrant:~$ ip -d link show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00 promiscuity 0 minmtu 0 maxmtu 0 addrgenmode eui64 numtxqueues 1 numrxqueues 1 gso_max_size 65536 gso_max_segs 65535 
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 08:00:27:b1:28:5d brd ff:ff:ff:ff:ff:ff promiscuity 0 minmtu 46 maxmtu 16110 addrgenmode eui64 numtxqueues 1 numrxqueues 1 gso_max_size 65536 gso_max_segs 65535 

3: eth0.15@eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DEFAULT group default qlen 1000
    link/ether 08:00:27:b1:28:5d brd ff:ff:ff:ff:ff:ff promiscuity 0 minmtu 0 maxmtu 65535 
    vlan protocol 802.1Q id 15 <REORDER_HDR> addrgenmode eui64 numtxqueues 1 numrxqueues 1 gso_max_size 65536 gso_max_segs 65535 
5: eth0.5@eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DEFAULT group default qlen 1000
    link/ether 08:00:27:b1:28:5d brd ff:ff:ff:ff:ff:ff promiscuity 0 minmtu 0 maxmtu 65535 
    vlan protocol 802.1Q id 5 <REORDER_HDR> addrgenmode eui64 numtxqueues 1 numrxqueues 1 gso_max_size 65536 gso_max_segs 65535
```


Также в Ubuntu 20.04 может быть сконфигурировано через YAML-файл Netplan:

```
network:
  ethernets:
    eth0:
      dhcp4: false
      addresses:
        - 192.168.122.201/24
      gateway4: 192.168.122.1
      nameservers:
          addresses: [8.8.8.8, 1.1.1.1]
    vlans:
        eth0.100:
            id: 100
            link: eth0
            addresses: [192.168.100.2/24]
```


4. Какие типы агрегации интерфейсов есть в Linux? Какие опции есть для балансировки нагрузки? Приведите пример конфига.


Типы агрегации:

- LACP (Link Aggregation Control Protocol) стандартный протокол

- Статическое агрегирование без использования протоколов



Mode-0(balance-rr) – Данный режим используется по умолчанию. Balance-rr обеспечивается балансировку нагрузки и отказоустойчивость. В данном режиме сетевые пакеты отправляются “по кругу”, от первого интерфейса к последнему. Если выходят из строя интерфейсы, пакеты отправляются на остальные оставшиеся. Дополнительной настройки коммутатора не требуется при нахождении портов в одном коммутаторе. При разностных коммутаторах требуется дополнительная настройка.

Mode-1(active-backup) – Один из интерфейсов работает в активном режиме, остальные в ожидающем. При обнаружении проблемы на активном интерфейсе производится переключение на ожидающий интерфейс. Не требуется поддержки от коммутатора.

Mode-2(balance-xor) – Передача пакетов распределяется по типу входящего и исходящего трафика по формуле ((MAC src) XOR (MAC dest)) % число интерфейсов. Режим дает балансировку нагрузки и отказоустойчивость. Не требуется дополнительной настройки коммутатора/коммутаторов.

Mode-3(broadcast) – Происходит передача во все объединенные интерфейсы, тем самым обеспечивая отказоустойчивость. Рекомендуется только для использования MULTICAST трафика.

Mode-4(802.3ad) – динамическое объединение одинаковых портов. В данном режиме можно значительно увеличить пропускную способность входящего так и исходящего трафика. Для данного режима необходима поддержка и настройка коммутатора/коммутаторов.

Mode-5(balance-tlb) – Адаптивная балансировки нагрузки трафика. Входящий трафик получается только активным интерфейсом, исходящий распределяется в зависимости от текущей загрузки канала каждого интерфейса. Не требуется специальной поддержки и настройки коммутатора/коммутаторов.

Mode-6(balance-alb) – Адаптивная балансировка нагрузки. Отличается более совершенным алгоритмом балансировки нагрузки чем Mode-5). Обеспечивается балансировку нагрузки как исходящего так и входящего трафика. Не требуется специальной поддержки и настройки коммутатора/коммутаторов.

```
vagrant@vagrant:~$ sudo modprobe bonding
vagrant@vagrant:~$ sudo apt install ifenslave
vagrant@vagrant:~$ sudo ifconfig eth0 down
vagrant@vagrant:~$ sudo ifconfig eth1 down
vagrant@vagrant:~$ sudo ip link add bond0 type bond mode 802.3ad
vagrant@vagrant:~$ sudo ip link set eth0 master bond0 
vagrant@vagrant:~$ sudo ip link set eth1 master bond0
```

Также в Ubuntu 20.04 через netplan:
```
network:
    version: 2
    renderer: networkd
    bonds:
        bond0:
            dhcp4: yes
            interfaces:
                - eth0
                - eth1
            parameters:
                mode: active-backup
                primary: eth0
```


5. Сколько IP адресов в сети с маской /29 ? Сколько /29 подсетей можно получить из сети с маской /24. Приведите несколько примеров /29 подсетей внутри сети 10.10.10.0/24.

6 IP-адресов хостов, IP-адрес сети и broadcast IP-адрес.
Из сети с маской /24 можно получить 32 подсети с маской /29 по 6 хостов в каждой подсети.

Примеры подсетей:`10.10.10.8/29`, `10.10.10.16/29`, `10.10.10.24/29`.


6. Задача: вас попросили организовать стык между 2-мя организациями. Диапазоны 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16 уже заняты. Из какой подсети допустимо взять частные IP адреса? Маску выберите из расчета максимум 40-50 хостов внутри подсети.

Допустимо взять из сети `100.64.0.0/10`.

`100.64.0.0/26`.


7. Как проверить ARP таблицу в Linux, Windows? Как очистить ARP кеш полностью? Как из ARP таблицы удалить только один нужный IP?

В Windows, в Linux - `arp -a`.

Удалить кэш-таблицу в Windows: `netsh interface IP delete arpcache` или `arp -d`.

Удалить только нужный ip в Windows: `arp -d <ip-address>`.

Удалить кэш-таблицу в Linux:`sudo ip neigh flush all`.

Удалить только нужный ip в Linux:`sudo arp -d <ip-address>`.

