# Домашнее задание к занятию "5.3. Введение. Экосистема. Архитектура. Жизненный цикл Docker контейнера"

---

## Задача 1

Сценарий выполения задачи:

- создайте свой репозиторий на https://hub.docker.com;
- выберете любой образ, который содержит веб-сервер Nginx;
- создайте свой fork образа;
- реализуйте функциональность:
запуск веб-сервера в фоне с индекс-страницей, содержащей HTML-код ниже:
```
<html>
<head>
Hey, Netology
</head>
<body>
<h1>I’m DevOps Engineer!</h1>
</body>
</html>
```
Опубликуйте созданный форк в своем репозитории и предоставьте ответ в виде ссылки на https://hub.docker.com/username_repo.

`https://hub.docker.com/repository/docker/naumdevops/nginx_index`

## Задача 2

Посмотрите на сценарий ниже и ответьте на вопрос:
"Подходит ли в этом сценарии использование Docker контейнеров или лучше подойдет виртуальная машина, физическая машина? Может быть возможны разные варианты?"

Детально опишите и обоснуйте свой выбор.

--

Сценарий:

- Высоконагруженное монолитное java веб-приложение - виртуальная машина для упрощения доступа к ресурсам из-за нагрузки. Docker-контейнеры больше подходят под микросервисы, а тут приложение монолитное.
- Nodejs веб-приложение - Docker контейнеры подходят, дадут упрощение в разработке, контроле версий, управлении/сопровождении и т.д., позволят обеспечить запуск приложений в разных средах.
- Мобильное приложение c версиями для Android и iOS -  уровень хранения данных на физической или виртуальной машине, уровень приложений и презентации - на микросервисной архитектуре с использованием Docker-контейнеров.
- Шина данных на базе Apache Kafka - скорее виртуальные сервера. Kafka - stateful сервис, выигрывающий от быстрого доступа к диску, Docker-контейнеры здесь не очень подходят, если речь о продуктивной среде.
- Elasticsearch кластер для реализации логирования продуктивного веб-приложения - три ноды elasticsearch, два logstash и две ноды kibana - docker-контейнеры (простота развертывания) или виртуальные машины. 
- Мониторинг-стек на базе Prometheus и Grafana -  Docker контейнеры подходят, так как не хранят никаких данных, легче развертывать/масштабировать;
- MongoDB, как основное хранилище данных для java-приложения - виртуальная или физическая машина, так как нужно хранить данные (контейнер не подходит);
- Gitlab сервер для реализации CI/CD процессов и приватный (закрытый) Docker Registry - виртуальная машина, так как предполагается хранение данных. Для тестирования возможно и использование Docker.

## Задача 3

- Запустите первый контейнер из образа ***centos*** c любым тэгом в фоновом режиме, подключив папку ```/data``` из текущей рабочей директории на хостовой машине в ```/data``` контейнера;
- Запустите второй контейнер из образа ***debian*** в фоновом режиме, подключив папку ```/data``` из текущей рабочей директории на хостовой машине в ```/data``` контейнера;
- Подключитесь к первому контейнеру с помощью ```docker exec``` и создайте текстовый файл любого содержания в ```/data```;
- Добавьте еще один файл в папку ```/data``` на хостовой машине;
- Подключитесь во второй контейнер и отобразите листинг и содержание файлов в ```/data``` контейнера.

```
naum@nubu:~$ sudo -i
[sudo] password for naum:
root@nubu:~# docker run -v $(pwd)/data:/data --name centos_con -dit centos
Unable to find image 'centos:latest' locally
latest: Pulling from library/centos
a1d0c7532777: Pull complete
Digest: sha256:a27fd8080b517143cbbbab9dfb7c8571c40d67d534bbdee55bd6c473f432b177
Status: Downloaded newer image for centos:latest
560f0100dd43670e0719d598a5a496175d830762742de091354253bf91e3f3a9
root@nubu:~#
root@nubu:~#
root@nubu:~# docker run -v $(pwd)/data:/data --name debian_con -dit debian
Unable to find image 'debian:latest' locally
latest: Pulling from library/debian
6aefca2dc61d: Pull complete
Digest: sha256:6846593d7d8613e5dcc68c8f7d8b8e3179c7f3397b84a47c5b2ce989ef1075a0
Status: Downloaded newer image for debian:latest
5afc3447a09c18b8d05661b78cb97432078ea5a0ff5fe64f33df0c9ca9216bae
root@nubu:~#
root@nubu:~#
root@nubu:~# docker exec -it centos_con /bin/bash
[root@560f0100dd43 /]# touch /data/file1
[root@560f0100dd43 /]# echo 'newline'>/data/file1
[root@560f0100dd43 /]# exit
exit
root@nubu:~#
root@nubu:~# cat data/file1
newline
root@nubu:~#
root@nubu:~# touch data/file2
root@nubu:~# echo 'secondline'>data/file2
root@nubu:~#
root@nubu:~# docker exec -it debian_con /bin/bash
root@5afc3447a09c:/# ls /data/
file1  file2
root@5afc3447a09c:/# cat /data/file1
newline
root@5afc3447a09c:/# cat /data/file2
secondline
```

## Задача 4 (*)

Воспроизвести практическую часть лекции самостоятельно.

Соберите Docker образ с Ansible, загрузите на Docker Hub и пришлите ссылку вместе с остальными ответами к задачам.

`https://hub.docker.com/repository/docker/naumdevops/ans`

---

