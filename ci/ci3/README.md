# Домашнее задание к занятию "08.03 Использование Yandex Cloud"

1. Допишите playbook: нужно сделать ещё один play, который устанавливает и настраивает lighthouse.
```
---
- name: Install Clickhouse
  hosts: clickhouse
  handlers:
    - name: Start clickhouse service
      become: true
      ansible.builtin.service:
        name: clickhouse-server
        state: restarted
  tasks:
    - block:
        - name: Get clickhouse distrib
          ansible.builtin.get_url:
            url: "https://packages.clickhouse.com/rpm/stable/{{ item }}-{{ clickhouse_version }}.noarch.rpm"
            dest: "./{{ item }}-{{ clickhouse_version }}.rpm"
          with_items: "{{ clickhouse_packages }}"
      rescue:
        - name: Get clickhouse distrib
          ansible.builtin.get_url:
            url: "https://packages.clickhouse.com/rpm/stable/clickhouse-common-static-{{ clickhouse_version }}.x86_64.rpm"
            dest: "./clickhouse-common-static-{{ clickhouse_version }}.rpm"
    - name: Install clickhouse packages
      become: true
      ansible.builtin.yum:
        name:
          - clickhouse-common-static-{{ clickhouse_version }}.rpm
          - clickhouse-client-{{ clickhouse_version }}.rpm
          - clickhouse-server-{{ clickhouse_version }}.rpm
      notify: Start clickhouse service
    - name: flush handlers
      ansible.builtin.meta: flush_handlers
    - name: Create database
      ansible.builtin.command: "clickhouse-client -q 'create database logs;'"
      register: create_db
      failed_when: create_db.rc != 0 and create_db.rc !=82
      changed_when: create_db.rc == 0
- name: Install vector
  hosts: vector
  tasks:
    - name: folder
      become: true
      file:
        state: directory
        path: /tmp/vector
      tags: vector
    - name: get vector package
      become: true
      ansible.builtin.get_url:
        url: "https://packages.timber.io/vector/0.24.1/vector-0.24.1-x86_64-unknown-linux-gnu.tar.gz"
        dest: "/tmp/vector-0.24.1-x86_64-unknown-linux-gnu.tar.gz"
      tags: vector
    - name: unpack vector
      become: true
      ansible.builtin.unarchive:
        copy: false
        src: /tmp/vector-0.24.1-x86_64-unknown-linux-gnu.tar.gz
        dest: /tmp/vector
        extra_opts: [--strip-components=2]
      tags: vector
    - name: Set environment Vector
      become: true
      template:
        src: templates/vector.sh.j2
        dest: /etc/profile.d/vector.sh
      tags: vector
    - name: Set vector systemd unit
      become: true
      ansible.builtin.template:
        src: templates/vector.service.j2
        dest: /etc/systemd/system/vector.service
        mode: "0664"
        owner: "{{ ansible_user_id }}"
        group: "{{ ansible_user_gid }}"
    - name: vector start service
      become: true
      ansible.builtin.systemd:
        name: vector
        state: started
        daemon_reload: true
- name: install nginx
  hosts: lighthouse
  handlers:
    - name: start-nginx
      become: true
      command: nginx
    - name: reload-nginx
      become: true
      command: nginx reload
  tasks:
    - name: NGINX | Install epel-release
      become: true
      ansible.builtin.yum:
        name: epel-release
        state: present
    - name: NGINX | Install NGINX
      become: true
      ansible.builtin.yum:
        name: nginx
        state: present
    - name: NGINX | create general config
      become: true
      template:
        src: templates/nginx.conf.j2
        dest: /etc/nginx/nginx.conf
        mode: 0664
      notify: reload-nginx
- name: Install lighthouse
  hosts: lighthouse
  handlers:
    - name: reload-nginx
      become: true
      command: nginx reload
  pre_tasks:
    - name: lighthouse | install deps
      become: true
      ansible.builtin.yum:
        name: git
        state: present
  tasks:
    - name: lighthouse | copy from git
      git:
        repo: "{{ lighthouse_vcs }}"
        version: master
        dest: "{{ lighthouse_location_dir }}"
    - name: Lighthouse | create Lighthouse config
      become: true
      template:
        src: templates/lighthouse.conf.j2
        dest: /etc/nginx/conf.d/default.conf
        mode: 0664
      notify: reload-nginx
```
2. При создании tasks рекомендую использовать модули: `get_url`, `template`, `yum`, `apt`.
    
    выполнено

3. Tasks должны: скачать статику lighthouse, установить nginx или любой другой webserver, настроить его конфиг для открытия lighthouse, запустить webserver.

    выполнено
    
4. Приготовьте свой собственный inventory файл `prod.yml`.
```
---
clickhouse:
  hosts:
    clickhouse-01:
      ansible_host: 51.250.99.159
vector:
  hosts:
    vector-01:
      ansible_host: 84.201.167.139
lighthouse:
  hosts:
    lighthouse-01:
      ansible_host: 84.201.137.14
```
5. Запустите `ansible-lint site.yml` и исправьте ошибки, если они есть.
    
    исправлены Trailing whitespace

6. Попробуйте запустить playbook на этом окружении с флагом `--check`.

выполнено

7. Запустите playbook на `prod.yml` окружении с флагом `--diff`. Убедитесь, что изменения на системе произведены.

выполнено

8. Повторно запустите playbook с флагом `--diff` и убедитесь, что playbook идемпотентен.

выполнено

9. Подготовьте README.md файл по своему playbook. В нём должно быть описано: что делает playbook, какие у него есть параметры и теги.

```
Playbook устанавливает clickhouse, vector и lighthouse.
inventory - определены хосты, на которые производится установка
group_vars - определены переменные для установки clickhouse, lighthouse

Описание playbook
Install Clickhouse: 
- скачивание rpm-пакеты для установки Clickhouse в блоке
- в случае неудачи при скачивании пакета в блоке, переход в rescue
- установка пакетов
- вызов handler для запуска сервиса clickhouse
- создание бд в случае если не создана ранее
Install Vector:
- создание каталога для установки vector
- получение архива для установки vector
- распаковка архива в ранее созданный каталог 
- добавление переменных окружения для работы с vector
- подготовка сервиса vector и его запуск
Install NGING:
- установка epel-release и nginx
- подготовка конфига nginх, перезагрузка конфига
Install lighthouse:
- установка git
- копирование файлов lighthouse с git
- подготовка конфига 
```
10. Готовый playbook выложите в свой репозиторий, поставьте тег `08-ansible-03-yandex` на фиксирующий коммит, в ответ предоставьте ссылку на него.

[Ссылка] (https://gitlab.com/naumdevops/devops-netology/-/tree/main/ci/ci3)



---
