# Домашнее задание к занятию "08.02 Работа с Playbook"

## Основная часть

1. Приготовьте свой собственный inventory файл `prod.yml`.
```
---
clickhouse:
  hosts:
    clickhouse-01:
      ansible_host: 51.250.96.83
vector:
  hosts:
    vector-01:
      ansible_host: 51.250.31.114
```
2. Допишите playbook: нужно сделать ещё один play, который устанавливает и настраивает [vector](https://vector.dev).
```
---
- name: Install Clickhouse
  hosts: clickhouse
  handlers:
    - name: Start clickhouse service
      become: true
      ansible.builtin.service:
        name: clickhouse-server
        state: restarted
  tasks:
    - block:
        - name: Get clickhouse distrib
          ansible.builtin.get_url:
            url: "https://packages.clickhouse.com/rpm/stable/{{ item }}-{{ clickhouse_version }}.noarch.rpm"
            dest: "./{{ item }}-{{ clickhouse_version }}.rpm"
          with_items: "{{ clickhouse_packages }}"
      rescue:
        - name: Get clickhouse distrib
          ansible.builtin.get_url:
            url: "https://packages.clickhouse.com/rpm/stable/clickhouse-common-static-{{ clickhouse_version }}.x86_64.rpm"
            dest: "./clickhouse-common-static-{{ clickhouse_version }}.rpm"
    - name: Install clickhouse packages
      become: true
      ansible.builtin.yum:
        name:
          - clickhouse-common-static-{{ clickhouse_version }}.rpm
          - clickhouse-client-{{ clickhouse_version }}.rpm
          - clickhouse-server-{{ clickhouse_version }}.rpm
      notify: Start clickhouse service
    - name: Create database
      ansible.builtin.command: "clickhouse-client -q 'create database logs;'"
      register: create_db
      failed_when: create_db.rc != 0 and create_db.rc !=82
      changed_when: create_db.rc == 0
- name: Install vector
  hosts: vector
  tasks:
    - name: folder
      become: true
      file:
        state: directory
        path: /tmp/vector
      tags: vector
    - name: get vector package
      become: true
      ansible.builtin.get_url:
        url: "https://packages.timber.io/vector/0.24.1/vector-0.24.1-x86_64-unknown-linux-gnu.tar.gz"
        dest: "/tmp/vector-0.24.1-x86_64-unknown-linux-gnu.tar.gz"
      tags: vector
    - name: unpack vector
      become: true
      ansible.builtin.unarchive:
        copy: false
        src: /tmp/vector-0.24.1-x86_64-unknown-linux-gnu.tar.gz
        dest: /tmp/vector
        extra_opts: [--strip-components=2]
      tags: vector
    - name: Set environment Vector
      become: true
      template:
        src: templates/vector.sh.j2
        dest: /etc/profile.d/vector.sh
      tags: vector
```
3. При создании tasks рекомендую использовать модули: `get_url`, `template`, `unarchive`, `file`.

выполнено

4. Tasks должны: скачать нужной версии дистрибутив, выполнить распаковку в выбранную директорию, установить vector.

выполнено

5. Запустите `ansible-lint site.yml` и исправьте ошибки, если они есть.

ошибок не было

6. Попробуйте запустить playbook на этом окружении с флагом `--check`.

выполнено

7. Запустите playbook на `prod.yml` окружении с флагом `--diff`. Убедитесь, что изменения на системе произведены.

выполнено

8. Повторно запустите playbook с флагом `--diff` и убедитесь, что playbook идемпотентен.

выполнено

9. Подготовьте README.md файл по своему playbook. В нём должно быть описано: что делает playbook, какие у него есть параметры и теги.
```
Playbook устанавливает clickhouse и vector.
inventory - определены хосты, на которые производится установка
group_vars - определены переменные для установки clickhouse

Описание playbook
Install Clickhouse: 
- скачивание rpm-пакеты для установки Clickhouse в блоке
- в случае неудачи при скачивании пакета в блоке, переход в rescue
- установка пакетов
- вызов handler для запуска сервиса clickhouse
- создание бд в случае если не создана ранее
Install Vector:
- создание каталога для установки vector
- получение архива для установки vector
- распаковка архива в ранее созданный каталог 
- добавление переменных окружения для работы с vector

```


10. Готовый playbook выложите в свой репозиторий, поставьте тег `08-ansible-02-playbook` на фиксирующий коммит, в ответ предоставьте ссылку на него.

[Ссылка] (https://gitlab.com/naumdevops/devops-netology/-/tree/main/ci/ci2/pl2)

---