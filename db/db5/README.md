# Домашнее задание к занятию "6.5. Elasticsearch"

## Задача 1

В этом задании вы потренируетесь в:
- установке elasticsearch
- первоначальном конфигурировании elastcisearch
- запуске elasticsearch в docker

Используя докер образ [elasticsearch:7](https://hub.docker.com/_/elasticsearch) как базовый:

- составьте Dockerfile-манифест для elasticsearch
- соберите docker-образ и сделайте `push` в ваш docker.io репозиторий
- запустите контейнер из получившегося образа и выполните запрос пути `/` c хост-машины

Требования к `elasticsearch.yml`:
- данные `path` должны сохраняться в `/var/lib` 
- имя ноды должно быть `netology_test`

В ответе приведите:
- текст Dockerfile манифеста
```
naum# cat Dockerfile 
FROM elasticsearch:7.17.4
COPY --chown=elasticsearch:elasticsearch elasticsearch.yml /usr/share/elasticsearch/config/
RUN mkdir /var/lib/logs \
    && chown elasticsearch:elasticsearch /var/lib/logs \
    && mkdir /var/lib/data \
    && chown elasticsearch:elasticsearch /var/lib/data

naum# cat elasticsearch.yml 
cluster.name: "netology_test"
node.name: "netology_test"
discovery.type: single-node
path.data: /var/lib/data
path.logs: /var/lib/logs
network.host: 0.0.0.0
```

- ссылку на образ в репозитории dockerhub

`https://hub.docker.com/r/naumdevops/elasticsearch-custom`

`docker pull naumdevops/elasticsearch-custom`

- ответ `elasticsearch` на запрос пути `/` в json виде
```
naum# docker run -d --name el -p 9200:9200 -p 9300:9300 naumdevops/elasticsearch-custom
naum# curl -X GET localhost:9200
{
  "name" : "netology_test",
  "cluster_name" : "netology_test",
  "cluster_uuid" : "xUL1Qke3Rt2qSATcSVpkWQ",
  "version" : {
    "number" : "7.17.4",
    "build_flavor" : "default",
    "build_type" : "docker",
    "build_hash" : "79878662c54c886ae89206c685d9f1051a9d6411",
    "build_date" : "2022-05-18T18:04:20.964345128Z",
    "build_snapshot" : false,
    "lucene_version" : "8.11.1",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
```

## Задача 2

В этом задании вы научитесь:
- создавать и удалять индексы
- изучать состояние кластера
- обосновывать причину деградации доступности данных

Ознакомтесь с [документацией](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html) 
и добавьте в `elasticsearch` 3 индекса, в соответствии со таблицей:

| Имя | Количество реплик | Количество шард |
|-----|-------------------|-----------------|
| ind-1| 0 | 1 |
| ind-2 | 1 | 2 |
| ind-3 | 2 | 4 |

```
naum# curl -X PUT "localhost:9200/ind-1" -H 'Content-Type: application/json' -d'{ "settings": { "index": { "number_of_shards": 1,  "number_of_replicas": 0 }}}'
naum# curl -X PUT "localhost:9200/ind-2" -H 'Content-Type: application/json' -d'{ "settings": { "index": { "number_of_shards": 4,  "number_of_replicas": 2 }}}'
naum# curl -X PUT "localhost:9200/ind-3" -H 'Content-Type: application/json' -d'{ "settings": { "index": { "number_of_shards": 4,  "number_of_replicas": 2 }}}'
```

Получите список индексов и их статусов, используя API и **приведите в ответе** на задание.
```
naum# curl 'localhost:9200/_cat/indices?v'
health status index            uuid                   pri rep docs.count docs.deleted store.size pri.store.size
green  open   .geoip_databases Eb3wmWvvSr2rMKsKJceyDg   1   0         41            0     38.9mb         38.9mb
green  open   ind-1            THfH8T02RGesWkR4Zc_1tw   1   0          0            0       226b           226b
yellow open   ind-3            laDJbNbQTKe9fv38S9X7VA   4   2          0            0       904b           904b
yellow open   ind-2            MALleSvYSrKAMS8U1y0muw   2   1          0            0       452b           452b
```

Получите состояние кластера `elasticsearch`, используя API.
```
naum# curl -X GET 'localhost:9200/_cluster/health/?pretty'
{
  "cluster_name" : "netology_test",
  "status" : "yellow",
  "timed_out" : false,
  "number_of_nodes" : 1,
  "number_of_data_nodes" : 1,
  "active_primary_shards" : 10,
  "active_shards" : 10,
  "relocating_shards" : 0,
  "initializing_shards" : 0,
  "unassigned_shards" : 10,
  "delayed_unassigned_shards" : 0,
  "number_of_pending_tasks" : 0,
  "number_of_in_flight_fetch" : 0,
  "task_max_waiting_in_queue_millis" : 0,
  "active_shards_percent_as_number" : 50.0
}
```
Как вы думаете, почему часть индексов и кластер находится в состоянии yellow?

Только 1 нода, а Elasticsearch не назначает реплику тому же узлу, что и основной шард. В паре созданных ранее индексов указано кол-во реплик.

Удалите все индексы.

```
naum# curl -X DELETE 'http://localhost:9200/ind-1'       
{"acknowledged":true}#                                                                                                                                        
naum# curl -X DELETE 'http://localhost:9200/ind-2'
{"acknowledged":true}#                                                                                                                                        
naum# curl -X DELETE 'http://localhost:9200/ind-3'
{"acknowledged":true}
#
naum# curl -X GET 'http://localhost:9200/_cat/indices?v'
health status index            uuid                   pri rep docs.count docs.deleted store.size pri.store.size
green  open   .geoip_databases Eb3wmWvvSr2rMKsKJceyDg   1   0         41            0     38.9mb         38.9mb
```

**Важно**

При проектировании кластера elasticsearch нужно корректно рассчитывать количество реплик и шард,
иначе возможна потеря данных индексов, вплоть до полной, при деградации системы.

## Задача 3

В данном задании вы научитесь:
- создавать бэкапы данных
- восстанавливать индексы из бэкапов

Создайте директорию `{путь до корневой директории с elasticsearch в образе}/snapshots`.

Используя API [зарегистрируйте](https://www.elastic.co/guide/en/elasticsearch/reference/current/snapshots-register-repository.html#snapshots-register-repository) 
данную директорию как `snapshot repository` c именем `netology_backup`.

**Приведите в ответе** запрос API и результат вызова API для создания репозитория.
```
naum# curl -X PUT "localhost:9200/_snapshot/netology_backup?pretty" -H 'Content-Type: application/json' -d'{"type": "fs", "settings": {"location": "/usr/share/elasticsearch/snapshots" }}'
{
  "acknowledged" : true
}
```

Создайте индекс `test` с 0 реплик и 1 шардом и **приведите в ответе** список индексов.

```
naum# curl -X PUT "localhost:9200/test" -H 'Content-Type: application/json' -d'{ "settings": { "index": { "number_of_shards": 1,  "number_of_replicas": 0 }}}'
{"acknowledged":true,"shards_acknowledged":true,"index":"test"}%

naum# curl 'localhost:9200/_cat/indices?v'
health status index            uuid                   pri rep docs.count docs.deleted store.size pri.store.size
green  open   .geoip_databases B29opYGJThG21GW5Xipu3A   1   0         41            0     38.9mb         38.9mb
green  open   test             gbxsbSzHTpSCYvHl14CEaQ   1   0          0            0       226b           226b
```

[Создайте `snapshot`](https://www.elastic.co/guide/en/elasticsearch/reference/current/snapshots-take-snapshot.html) 
состояния кластера `elasticsearch`.

**Приведите в ответе** список файлов в директории со `snapshot`ами.

```
naum# curl -X PUT "localhost:9200/_snapshot/netology_backup/snapshot2?pretty" -H 'Content-Type: application/json' -d' {"include_global_state": true}}'
{
  "accepted" : true
}
naum# sudo docker exec -it el bash
root@6047b1ba943c:/usr/share/elasticsearch# ls /usr/share/elasticsearch/snapshots/
index-8  index.latest  indices  meta-LeirT9nVQxG8-GyUgNzJEg.dat  snap-LeirT9nVQxG8-GyUgNzJEg.dat
```

Удалите индекс `test` и создайте индекс `test-2`. **Приведите в ответе** список индексов.

```
naum# curl -X DELETE 'http://localhost:9200/test'
{"acknowledged":true}%
naum# curl -X PUT "localhost:9200/test-2" -H 'Content-Type: application/json' -d'{ "settings": { "index": { "number_of_shards": 1,  "number_of_replicas": 0 }}}'
{"acknowledged":true,"shards_acknowledged":true,"index":"test-2"}%
naum# curl 'localhost:9200/_cat/indices?v'
health status index            uuid                   pri rep docs.count docs.deleted store.size pri.store.size
green  open   .geoip_databases B29opYGJThG21GW5Xipu3A   1   0         41            0     38.9mb         38.9mb
green  open   test-2           Z6Ii1TXEQcSsTiehMBfEmA   1   0          0            0       226b           226b
```

[Восстановите](https://www.elastic.co/guide/en/elasticsearch/reference/current/snapshots-restore-snapshot.html) состояние
кластера `elasticsearch` из `snapshot`, созданного ранее. 

**Приведите в ответе** запрос к API восстановления и итоговый список индексов.

```
naum# curl -X POST "localhost:9200/_snapshot/netology_backup/snapshot2/_restore?wait_for_completion=true&pretty" -H 'Content-Type: application/json' -d'{"include_global_state": true}'

{
  "snapshot" : {
    "snapshot" : "snapshot2",
    "indices" : [
      ".geoip_databases",
      ".ds-ilm-history-5-2022.06.19-000001",
      "test",
      ".ds-.logs-deprecation.elasticsearch-default-2022.06.19-000001"
    ],
    "shards" : {
      "total" : 4,
      "failed" : 0,
      "successful" : 4
    }
  }
}

naum# curl 'localhost:9200/_cat/indices?v'
health status index            uuid                   pri rep docs.count docs.deleted store.size pri.store.size
green  open   test-2           Z6Ii1TXEQcSsTiehMBfEmA   1   0          0            0       226b           226b
green  open   .geoip_databases ugmWVHl5RsikJGVkrLu9sw   1   0         41            0     38.9mb         38.9mb
green  open   test             V35LSmTrQ1S3Y0dMM-gM7g   1   0          0            0       226b           226b
```

Подсказки:
- возможно вам понадобится доработать `elasticsearch.yml` в части директивы `path.repo` и перезапустить `elasticsearch`

---