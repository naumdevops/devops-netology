# Домашнее задание к занятию "6.4. PostgreSQL"

## Задача 1

Используя docker поднимите инстанс PostgreSQL (версию 13). Данные БД сохраните в volume.

Подключитесь к БД PostgreSQL используя `psql`.

Воспользуйтесь командой `\?` для вывода подсказки по имеющимся в `psql` управляющим командам.

**Найдите и приведите** управляющие команды для:
- вывода списка БД - `\l`
```
postgres=# \l
                                 List of databases
   Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges

-----------+----------+----------+------------+------------+--------------------
---
 postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
 template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres
  +
           |          |          |            |            | postgres=CTc/postgr
es
 template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres
  +
           |          |          |            |            | postgres=CTc/postgr
es
(3 rows)
```

- подключения к БД `\c db_name`
```
postgres=# \c postgres
You are now connected to database "postgres" as user "postgres".
```

- вывода списка таблиц `\dtS`, S - показывает системные объекты.
```
postgres=# \dtS
                    List of relations
   Schema   |          Name           | Type  |  Owner
------------+-------------------------+-------+----------
 pg_catalog | pg_aggregate            | table | postgres
 pg_catalog | pg_am                   | table | postgres
 pg_catalog | pg_amop                 | table | postgres
 pg_catalog | pg_amproc               | table | postgres
 pg_catalog | pg_attrdef              | table | postgres
 pg_catalog | pg_attribute            | table | postgres
 pg_catalog | pg_auth_members         | table | postgres
 pg_catalog | pg_authid               | table | postgres
 pg_catalog | pg_cast                 | table | postgres
 pg_catalog | pg_class                | table | postgres
 ...
```

- вывода описания содержимого таблиц - `\d table_name`

```
postgres=# \d pg_user_mapping
         Table "pg_catalog.pg_user_mapping"
  Column   |  Type  | Collation | Nullable | Default
-----------+--------+-----------+----------+---------
 oid       | oid    |           | not null |
 umuser    | oid    |           | not null |
 umserver  | oid    |           | not null |
 umoptions | text[] | C         |          |
Indexes:
    "pg_user_mapping_oid_index" UNIQUE, btree (oid)
    "pg_user_mapping_user_server_index" UNIQUE, btree (umuser, umserver)
```

- выхода из psql `\q`


## Задача 2

Используя `psql` создайте БД `test_database`.

Изучите [бэкап БД](https://github.com/netology-code/virt-homeworks/tree/master/06-db-04-postgresql/test_data).

Восстановите бэкап БД в `test_database`.

Перейдите в управляющую консоль `psql` внутри контейнера.

Подключитесь к восстановленной БД и проведите операцию ANALYZE для сбора статистики по таблице.

Используя таблицу [pg_stats](https://postgrespro.ru/docs/postgresql/12/view-pg-stats), найдите столбец таблицы `orders` 
с наибольшим средним значением размера элементов в байтах.

**Приведите в ответе** команду, которую вы использовали для вычисления и полученный результат.
```
test_database=# analyze VERBOSE orders;
INFO:  analyzing "public.orders"
INFO:  "orders": scanned 1 of 1 pages, containing 8 live rows and 0 dead rows; 8 rows in sample, 8 estimated total rows
ANALYZE
test_database=# select max(avg_width) from pg_stats where tablename = 'orders';
 max 
-----
  16
(1 row)
```

## Задача 3

Архитектор и администратор БД выяснили, что ваша таблица orders разрослась до невиданных размеров и
поиск по ней занимает долгое время. Вам, как успешному выпускнику курсов DevOps в нетологии предложили
провести разбиение таблицы на 2 (шардировать на orders_1 - price>499 и orders_2 - price<=499).

Предложите SQL-транзакцию для проведения данной операции.

```
test_database=# BEGIN;
BEGIN
test_database=# create table new_orders (id integer, title text, price integer) partition by range(price);
CREATE TABLE
test_database=# create table orders_1 partition of new_orders for values from (500) to (maxvalue);
CREATE TABLE
test_database=# create table orders_2 partition of new_orders for values from (0) to (500);
CREATE TABLE
test_database=# insert into new_orders (id, title, price) select * from orders;
INSERT 0 8
test_database=# alter table orders rename to orders_backup;
ALTER TABLE
test_database=# alter table new_orders rename to orders;
ALTER TABLE
test_database=# COMMIT;
COMMIT
```

Можно ли было изначально исключить "ручное" разбиение при проектировании таблицы orders?

Да, можно было бы изначально спроектировать таблицу orders с партициями. 

## Задача 4

Используя утилиту `pg_dump` создайте бекап БД `test_database`.

`root@0a26c7f89617:/# pg_dump -U postgres -F p test_database > /var/lib/postgresql/test_database_sql`

Как бы вы доработали бэкап-файл, чтобы добавить уникальность значения столбца `title` для таблиц `test_database`?

Можно добавить атрибут unique для столбца (ограничение уникальности).

---
