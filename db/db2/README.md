# Домашнее задание к занятию "6.2. SQL"

## Введение

## Задача 1

Используя docker поднимите инстанс PostgreSQL (версию 12) c 2 volume, 
в который будут складываться данные БД и бэкапы.

Приведите получившуюся команду или docker-compose манифест.

```
naum# cat docker-compose.yml
version: "3.3"
services:
  pg:
    container_name: pg
    image: postgres:12
    environment:
      - POSTGRES_PASSWORD=postgres
    volumes:
      - ./pgdata:/var/lib/postgresql/data
      - ./pgbackup:/var/lib/postgresql
    ports:
      - "5432:5432"
    network_mode: bridge
naum#
naum# docker-compose -f docker-compose.yml up -d
Starting pg ... done
naum#
```

## Задача 2

В БД из задачи 1: 
- создайте пользователя test-admin-user и БД test_db
- в БД test_db создайте таблицу orders и clients (спeцификация таблиц ниже)
- предоставьте привилегии на все операции пользователю test-admin-user на таблицы БД test_db
- создайте пользователя test-simple-user  
- предоставьте пользователю test-simple-user права на SELECT/INSERT/UPDATE/DELETE данных таблиц БД test_db

Таблица orders:
- id (serial primary key)
- наименование (string)
- цена (integer)

Таблица clients:
- id (serial primary key)
- фамилия (string)
- страна проживания (string, index)
- заказ (foreign key orders)

Приведите:
- итоговый список БД после выполнения пунктов выше,
- описание таблиц (describe)
- SQL-запрос для выдачи списка пользователей с правами над таблицами test_db
- список пользователей с правами над таблицами test_db

```
naum# docker exec -it pg /bin/bash
root@64da666b6c37:/# psql -U postgres
psql (12.11 (Debian 12.11-1.pgdg110+1))
Type "help" for help.

postgres=# create user "test-admin-user";
CREATE ROLE
postgres=# create database "test_db";
CREATE DATABASE
postgres=# \c test_db
You are now connected to database "test_db" as user "postgres".
test_db=# create table orders (
    id SERIAL,
    name TEXT,
    price INTEGER,
    PRIMARY KEY(id)
);
CREATE TABLE
test_db=# create table clients (
    id SERIAL,
    lastname TEXT,
    country TEXT,
    orderid INTEGER,
    PRIMARY KEY(id),
    CONSTRAINT fk_order
        FOREIGN KEY(orderid) REFERENCES orders(id)
);
CREATE TABLE
test_db=# grant all on all tables in schema "public" to "test-admin-user";
GRANT
test_db=# create user "test-simple-user";
CREATE ROLE
test_db=# grant SELECT, INSERT, UPDATE, DELETE on orders, clients to "test-simple-user";
GRANT
test_db=# \l
                                 List of databases
   Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges
-----------+----------+----------+------------+------------+-----------------------
 postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
 template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
 test_db   | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
(4 rows)
test_db=#
test_db=# \d orders
                            Table "public.orders"
 Column |  Type   | Collation | Nullable |              Default
--------+---------+-----------+----------+------------------------------------
 id     | integer |           | not null | nextval('orders_id_seq'::regclass)
 name   | text    |           |          |
 price  | integer |           |          |
Indexes:
    "orders_pkey" PRIMARY KEY, btree (id)
Referenced by:
    TABLE "clients" CONSTRAINT "fk_order" FOREIGN KEY (orderid) REFERENCES orders(id)
test_db=#
test_db=#
test_db=# \d clients
                             Table "public.clients"
  Column  |  Type   | Collation | Nullable |               Default
----------+---------+-----------+----------+-------------------------------------
 id       | integer |           | not null | nextval('clients_id_seq'::regclass)
 lastname | text    |           |          |
 country  | text    |           |          |
 orderid  | integer |           |          |
Indexes:
    "clients_pkey" PRIMARY KEY, btree (id)
Foreign-key constraints:
    "fk_order" FOREIGN KEY (orderid) REFERENCES orders(id)

test_db=#
test_db=# SELECT table_catalog, grantee, table_schema, table_name, privilege_type
FROM information_schema.table_privileges
WHERE grantee in ('test-admin-user', 'test-simple-user');
 table_catalog |     grantee      | table_schema | table_name | privilege_type
---------------+------------------+--------------+------------+----------------
 test_db       | test-admin-user  | public       | orders     | INSERT
 test_db       | test-admin-user  | public       | orders     | SELECT
 test_db       | test-admin-user  | public       | orders     | UPDATE
 test_db       | test-admin-user  | public       | orders     | DELETE
 test_db       | test-admin-user  | public       | orders     | TRUNCATE
 test_db       | test-admin-user  | public       | orders     | REFERENCES
 test_db       | test-admin-user  | public       | orders     | TRIGGER
 test_db       | test-simple-user | public       | orders     | INSERT
 test_db       | test-simple-user | public       | orders     | SELECT
 test_db       | test-simple-user | public       | orders     | UPDATE
 test_db       | test-simple-user | public       | orders     | DELETE
 test_db       | test-admin-user  | public       | clients    | INSERT
 test_db       | test-admin-user  | public       | clients    | SELECT
 test_db       | test-admin-user  | public       | clients    | UPDATE
 test_db       | test-admin-user  | public       | clients    | DELETE
 test_db       | test-admin-user  | public       | clients    | TRUNCATE
 test_db       | test-admin-user  | public       | clients    | REFERENCES
 test_db       | test-admin-user  | public       | clients    | TRIGGER
 test_db       | test-simple-user | public       | clients    | INSERT
 test_db       | test-simple-user | public       | clients    | SELECT
 test_db       | test-simple-user | public       | clients    | UPDATE
 test_db       | test-simple-user | public       | clients    | DELETE
(22 rows)

```

## Задача 3

Используя SQL синтаксис - наполните таблицы следующими тестовыми данными:

Таблица orders

|Наименование|цена|
|------------|----|
|Шоколад| 10 |
|Принтер| 3000 |
|Книга| 500 |
|Монитор| 7000|
|Гитара| 4000|

Таблица clients

|ФИО|Страна проживания|
|------------|----|
|Иванов Иван Иванович| USA |
|Петров Петр Петрович| Canada |
|Иоганн Себастьян Бах| Japan |
|Ронни Джеймс Дио| Russia|
|Ritchie Blackmore| Russia|

Используя SQL синтаксис:
- вычислите количество записей для каждой таблицы 
- приведите в ответе:
    - запросы 
    - результаты их выполнения.

```
test_db=# insert into orders(name, price) values ('Шоколад', 10), ('Принтер', 3000), ('Книга', 500), ('Монитор', 7000), ('Гитара', 4000);
INSERT 0 5
test_db=# insert into clients(lastname, country) values ('Иванов Иван Иванович', 'USA'), ('Петров Петр Петрович', 'Canada'), ('Иоганн Себастьян Бах', 'Japan'), ('Ронни Джеймс Дио', 'Russia'), ('Ritchie Blackmore', 'Russia');
INSERT 0 5
test_db=# select count(*) from clients;
 count
-------
     5
(1 row)

test_db=# select count(*) from orders;
 count
-------
     5
(1 row)

test_db=# select * from clients;
 id |       lastname       | country | orderid
----+----------------------+---------+---------
  1 | Иванов Иван Иванович | USA     |
  2 | Петров Петр Петрович | Canada  |
  3 | Иоганн Себастьян Бах | Japan   |
  4 | Ронни Джеймс Дио     | Russia  |
  5 | Ritchie Blackmore    | Russia  |
(5 rows)

test_db=# select * from orders;
 id |  name   | price
----+---------+-------
  1 | Шоколад |    10
  2 | Принтер |  3000
  3 | Книга   |   500
  4 | Монитор |  7000
  5 | Гитара  |  4000
(5 rows)

```

## Задача 4

Часть пользователей из таблицы clients решили оформить заказы из таблицы orders.

Используя foreign keys свяжите записи из таблиц, согласно таблице:

|ФИО|Заказ|
|------------|----|
|Иванов Иван Иванович| Книга |
|Петров Петр Петрович| Монитор |
|Иоганн Себастьян Бах| Гитара |

Приведите SQL-запросы для выполнения данных операций.

Приведите SQL-запрос для выдачи всех пользователей, которые совершили заказ, а также вывод данного запроса.
 
Подсказк - используйте директиву `UPDATE`.

```
test_db=# update clients set orderid = 3 where id = 1;
UPDATE 1
test_db=# update clients set orderid = 4 where id = 2;
UPDATE 1
test_db=# update clients set orderid = 5 where id = 3;
UPDATE 1
test_db=# select * from clients where orderid is not null;
 id |       lastname       | country | orderid
----+----------------------+---------+---------
  1 | Иванов Иван Иванович | USA     |       3
  2 | Петров Петр Петрович | Canada  |       4
  3 | Иоганн Себастьян Бах | Japan   |       5
(3 rows)

```

## Задача 5

Получите полную информацию по выполнению запроса выдачи всех пользователей из задачи 4 
(используя директиву EXPLAIN).

Приведите получившийся результат и объясните что значат полученные значения.

```
test_db=# explain select * from clients where orderid is not null;
                        QUERY PLAN
-----------------------------------------------------------
 Seq Scan on clients  (cost=0.00..18.10 rows=806 width=72)
   Filter: (orderid IS NOT NULL)
(2 rows)
```


explain показывает какие таблицы будут сканироваться и как - индексное сканирование или последовательное.
в данном случае - последовательное.
также показывается ожидаемая стоимость выполнения оператора - сколько будет выполняться этот оператор 
(стоимость запуска до выдачи первой строки и общая стоимость выдачи всех строк).

## Задача 6

Создайте бэкап БД test_db и поместите его в volume, предназначенный для бэкапов (см. Задачу 1).

Остановите контейнер с PostgreSQL (но не удаляйте volumes).

Поднимите новый пустой контейнер с PostgreSQL.

Восстановите БД test_db в новом контейнере.

Приведите список операций, который вы применяли для бэкапа данных и восстановления. 

```
root@64da666b6c37:/#
root@64da666b6c37:/# pg_dump -U postgres -F p test_db > /var/lib/postgresql/test_db.sql
root@64da666b6c37:/#
root@64da666b6c37:/# exit
exit
naum#
naum# ls pgbackup
data  test_db.sql
naum#
naum# docker stop pg
pg
naum#
naum# cat docker-compose2.yml
version: "3.3"
services:
  pg_bak:
    container_name: pg_bak
    image: postgres:12
    environment:
      - POSTGRES_PASSWORD=postgres
    volumes:
      - ./pgbackup:/var/lib/postgresql
    ports:
      - "5432:5432"
    network_mode: bridge
naum# docker-compose -f docker-compose2.yml up -d
WARNING: Found orphan containers (pg) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
Creating pg_bak ... done
naum#
naum# docker exec -it pg_bak /bin/bash
root@b5fa2e0fda1e:/#
root@b5fa2e0fda1e:/# ls /var/lib/postgresql/
data  test_db.sql
root@b5fa2e0fda1e:/#
root@b5fa2e0fda1e:/# psql -U postgres
psql (12.11 (Debian 12.11-1.pgdg110+1))
Type "help" for help.

postgres=# create database test_db;
CREATE DATABASE
postgres=# exit
root@b5fa2e0fda1e:/#
root@b5fa2e0fda1e:/# psql -U postgres -d test_db -f /var/lib/postgresql/test_db.sql
SET
SET
SET
SET
SET
 set_config
------------

(1 row)

SET
SET
SET
SET
SET
SET
CREATE TABLE
ALTER TABLE
CREATE SEQUENCE
ALTER TABLE
ALTER SEQUENCE
CREATE TABLE
ALTER TABLE
CREATE SEQUENCE
ALTER TABLE
ALTER SEQUENCE
ALTER TABLE
ALTER TABLE
COPY 5
COPY 5
 setval
--------
      5
(1 row)

 setval
--------
      5
(1 row)

ALTER TABLE
ALTER TABLE
ALTER TABLE
psql:/var/lib/postgresql/test_db.sql:176: ERROR:  role "test-admin-user" does not exist
psql:/var/lib/postgresql/test_db.sql:177: ERROR:  role "test-simple-user" does not exist
psql:/var/lib/postgresql/test_db.sql:184: ERROR:  role "test-admin-user" does not exist
psql:/var/lib/postgresql/test_db.sql:185: ERROR:  role "test-simple-user" does not exist
root@b5fa2e0fda1e:/#
root@b5fa2e0fda1e:/# psql -U postgres
psql (12.11 (Debian 12.11-1.pgdg110+1))
Type "help" for help.

postgres=# \c test_db
You are now connected to database "test_db" as user "postgres".
test_db=# select * from clients;
 id |       lastname       | country | orderid
----+----------------------+---------+---------
  4 | Ронни Джеймс Дио     | Russia  |
  5 | Ritchie Blackmore    | Russia  |
  1 | Иванов Иван Иванович | USA     |       3
  2 | Петров Петр Петрович | Canada  |       4
  3 | Иоганн Себастьян Бах | Japan   |       5
(5 rows)

```

---